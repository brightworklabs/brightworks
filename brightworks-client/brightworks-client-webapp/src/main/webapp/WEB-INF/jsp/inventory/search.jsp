<%--
  Created by IntelliJ IDEA.
  User: Kurt
  Date: 2/19/2015
  Time: 5:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<jsp:include page="../includes/head.jsp" flush="true"/>
<body>
<!-- Header -->
<jsp:include page="../includes/page-header.jsp" flush="true" />

<!-- Content-->
<div class="container-fluid" id="container">

    <!-- Sidebar -->
    <jsp:include page="../includes/side-bar.jsp" flush="true" />

    <!-- Page content -->
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            Search
        </div><!--/.panel-heading-->

        <!-- Search Form -->
        <div class="panel-body">
            <div class="row">
                <div class="search-group centered col-md-8">
                    <form action="" class="search-form">
                        <div class="form-group has-feedback">
                            <label for="search" class="sr-only">Search</label>
                            <input type="text" class="form-control" name="search" id="search" placeholder="Search for Product Name and Product Code">
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </form>

                </div>
            </div><!--/.row-->

            <hr>
            <!-- Results -->
            <div class="row results-row">
                <table id="search-results" class="table table-striped table-bordered"  width="100%">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Item Code</th>
                        <th>Name</th>
                        <th>Suggested Retail Price</th>
                        <th>Dealers Price</th>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <th>Cost</th>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_CLERK')">
                            <th>Stocks</th>
                        </sec:authorize>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div><!--/.row-->
            <!---------------------------->
            <!--- Handlebars Template ---->

        </div><!--/.panel-body-->

    </div><!--/.panel-->


     <!-- Success Modal -->
    <div class="modal fade" id="addedToCheckOutModal">
        <div class="modal-dialog modal-vertical-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4><strong class="text-primary">Product Page</strong></h4>
                </div>

                <div class="modal-body">
                    Item Successfully Added to Checkout!
                </div>

                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>

                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/product/checkout">Proceed To Checkout <i class="fa fa-shopping-cart fa-sm icon-right"></i></a>
                </div>
            </div>
        </div>
    </div><!--/.modal-->

    <!-- Error Modal -->
    <div class="modal fade" id="noStockModal">
        <div class="modal-dialog modal-vertical-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4><strong class="text-primary">Product Page</strong></h4>
                </div>

                <div class="modal-body">
                    Item Out of Stock!
                </div>

                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>

                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/product/search">Browse Inventory <i class="fa fa-search fa-sm icon-right"></i></a>
                </div>
            </div>
        </div>
    </div><!--/.modal-->

</div><!--/.container-->

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- DataTables JS -->
<script src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>

<script src="//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<!-- Main JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>

<sec:authorize access="hasRole('ROLE_ADMIN')">
    <script>
        $(document).ready(function() {
            var resultsTable = $('#search-results').dataTable( {
                "bProcessing": false,
                "bServerSide": true,
                "sServerMethod": "GET",
                "sAjaxSource": "${pageContext.request.contextPath}/product/searchProduct",
                "columns":[
                    {"data":"id"},
                    {"data":"code"},
                    {"data":"name"},
                    {"data":"srp"},
                    {"data":"dp"},
                    {"data":"cost"}
                ],
                "aoColumnDefs":[{
                    "aTargets": [ 1 ],
                    "bSortable": false,
                    "mRender": function ( code, type, full )  {
                        return  '<a href="${pageContext.request.contextPath}/product/view/'+code+'">' + code + '</a>';
                    }
                },{
                    "aTargets": [ 2 ],
                    "bSortable": false
                },{
                    "aTargets": [ 3 ],
                    "bSortable": false
                },{
                    "aTargets": [ 4 ],
                    "bSortable": false
                },{
                    "aTargets": [ 5 ],
                    "bSortable": false
                }]
            });

            $("#search").keyup(function() {
                resultsTable.fnFilter(this.value);
            });
        });

        $(document).on('click','.checkout',function(e){
            e.preventDefault();
            var href = $(this).attr("href")
            $.ajax({
                url: href,
                success: function(e){
                    $("#addedToCheckOutModal").modal("show")
                },
                error:function(e){
                    $("#noStockModal").modal("show")
                }
            })
        });
    </script>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_CLERK')" >
<script>
    $(document).ready(function() {
        var resultsTable = $('#search-results').dataTable( {
            "bProcessing": false,
            "bServerSide": true,
            "sServerMethod": "GET",
            "sAjaxSource": "${pageContext.request.contextPath}/product/searchProduct",
            "columns":[
                {"data":"id"},
                {"data":"code"},
                {"data":"name"},
                {"data":"srp"},
                {"data":"dp"},
                {"data":"stock"},
                {"data":"code"}
            ],
            "aoColumnDefs":[{
                "aTargets": [ 1 ],
                "bSortable": false,
                "mRender": function ( code, type, full )  {
                    return  '<a href="${pageContext.request.contextPath}/product/view/'+code+'">' + code + '</a>';
                }
            },{
                "aTargets": [ 2 ],
                "bSortable": false
            },{
                "aTargets": [ 3 ],
                "bSortable": false
            },{
                "aTargets": [ 4 ],
                "bSortable": false
            },{
                "aTargets": [ 5 ],
                "bSortable": false
            },{
                "aTargets": [ 6 ],
                "bSortable": false,
                "mRender": function ( code, type ) {
                    return '<a class = "checkout" href="${pageContext.request.contextPath}/ajax/product/checkout?itemCode='+ code +'" style="text-decoration: none !important;">Add Item To Checkout <i class="fa fa-shopping-cart fa-sm icon-right"></i></a>'
                }

            }]
        });

        $("#search").keyup(function() {
            resultsTable.fnFilter(this.value);
        });
    });

    $(document).on('click','.checkout',function(e){
        e.preventDefault();
        var href = $(this).attr("href")
        $.ajax({
            url: href,
            success: function(e){
                $("#addedToCheckOutModal").modal("show")
            },
            error:function(e){
                $("#noStockModal").modal("show")
            }
        })
    });
</script>
</sec:authorize>
</body>
</html>
