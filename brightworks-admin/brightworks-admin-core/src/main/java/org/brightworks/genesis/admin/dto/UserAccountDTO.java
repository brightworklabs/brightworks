package org.brightworks.genesis.admin.dto;

import org.brightworks.genesis.admin.role.AccountRole;

/**
 * Created by kyeljohndavid on 4/3/2015.
 */
public class UserAccountDTO {

    private Long id;

    private String username;

    private String password;

    private String firstName;

    private String lastName;

    private AccountRole accountRole;

    /**
     * Where the account is registered
     */
    private String tenantCode;

    private String tenantDescription;

    public UserAccountDTO(){}

    public UserAccountDTO(Long id,String username, String firstName, String lastName, AccountRole role, String tenantDescription,String tenantCode) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountRole = role;
        this.tenantDescription = tenantDescription;
        this.tenantCode = tenantCode;
    }

    public String getTenantDescription() {
        return tenantDescription;
    }

    public void setTenantDescription(String tenantDescription) {
        this.tenantDescription = tenantDescription;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }


    public AccountRole getAccountRole() {
        return accountRole;
    }

    public void setAccountRole(AccountRole accountRole) {
        this.accountRole = accountRole;
    }
}
