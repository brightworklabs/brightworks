<%--
  Created by IntelliJ IDEA.
  User: Kurt
  Date: 2/19/2015
  Time: 5:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<jsp:include page="../includes/head.jsp" flush="true"/>
<body>
<!-- Header -->
<jsp:include page="../includes/page-header.jsp" flush="true" />

<!-- Content-->
<div class="container-fluid" id="container">
    <!-- Sidebar -->
    <jsp:include page="../includes/side-bar.jsp" flush="true" />
    <!-- Page content -->
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            Search Receipts/Transactions
        </div><!--/.panel-heading-->

        <!-- Search Form -->
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 col-sm-12 form-group">
                    <div class="col-md-6 col-sm-12 no-padding-left"><label>Date From</label><input type="date" class="form-control" id="dateFrom"></div>
                    <div class="col-md-6 col-sm-12 no-padding-right"><label>Date To</label><input type="date" class="form-control" id="dateTo"></div>
                </div>
            </div><!--/.row-->

            <hr>
            <!-- Results -->
            <div class="row results-row">
                <table id="search-results" class="table table-striped table-bordered"  width="100%">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Transaction ID</th>
                        <th>Transaction Number</th>
                        <th>Total Bill</th>
                    </tr>
                    </thead>
                </table>
            </div><!--/.row-->
            <!---------------------------->
            <!--- Handlebars Template ---->
        </div><!--/.panel-body-->
    </div><!--/.panel-->
</div><!--/.container-->

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- DataTables JS -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

<script src="//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<!-- Main JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>

<sec:authorize access="hasRole('ROLE_CLERK')" >
    <script>
        $(document).ready(function() {
            var resultsTable = $('#search-results').dataTable( {
                "bProcessing": true,
                "bServerSide": true,
                "sServerMethod": "GET",
                "sAjaxSource": "${pageContext.request.contextPath}/receipt/ajax/search",

                "fnServerParams": function (aoData) {
                    aoData.push({ "name": "fromDate", "value": $("#dateFrom").val()});
                    aoData.push({ "name": "toDate", "value": $('#dateTo').val()});
                },
                "columns":[
                    {"data":"formattedDate"},
                    {"data":"transactionId"},
                    {"data":"transactionNumber"},
                    {"data":"totalAmount"},
                ],
                "aoColumnDefs":[{
                    "aTargets": [ 0 ],
                    "bSortable": false
                },{
                    "aTargets": [ 1 ],
                    "bSortable": false
                },{
                    "aTargets": [ 2 ],
                    "bSortable": false,
                    "mRender": function ( number, type, full )  {
                        return  '<a href="${pageContext.request.contextPath}/receipt/'+number+'">' + number+ '</a>';
                    }
                },{
                    "aTargets": [ 3 ],
                    "bSortable": false
                }]
            });

            $("#search").keyup(function() {
                resultsTable.fnFilter(this.value);
            });

            $("#dateFrom").change(function(){
                resultsTable.fnFilter(this.value);
            });

            $("#dateTo").change(function(){
                resultsTable.fnFilter(this.value);
            })
        });
    </script>
</sec:authorize>
</body>
</html>
