package org.brightworks.genesis.inventory.mapper;

import org.brightworks.genesis.inventory.dto.ProductDTO;
import org.brightworks.genesis.inventory.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.factory.Mappers;

/**
 * Created by KyelDavid on 1/16/2015.
 */
@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface  ProductMapper {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    @Mappings(value = {
            @Mapping(source = "picture.id", target = "pictureId"),
            @Mapping(source = "type", target = "producType"),
            @Mapping(source = "picture.empty", target = "noImage"),
            @Mapping(source = "picture.picture",target = "picture", ignore = true),
    })
    ProductDTO toDto(Product product);
}
