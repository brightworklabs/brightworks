package org.brightworks.genesis.commons.security;

import org.brightworks.genesis.commons.security.dto.UserAuthDto;
import org.brightworks.genesis.commons.security.dto.UserAuthRequest;
import org.brightworks.genesis.commons.security.dto.UserAuthResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by kyel on 11/2/2014.
 */
@Component
public class TenantAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private TenantAccountAuthService tenantAccountAuthService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = (String) authentication.getPrincipal();

        UserAuthDto authDto = new UserAuthDto(username,password,new ArrayList<String>(),"");
        UserAuthRequest authRequest = new UserAuthRequest(authDto);
        /**
         * Should return a TENANT Account DTO
         *  that will help populate the tenant user details
         */
        try{
            UserAuthResponse authResponse = tenantAccountAuthService.authenticate(authDto);
            //TODO: Fix TenantUserDetails and Authentication token, refactor it
            return new TenantAuthenticationToken(new TenantUserDetails(),password,null);
        }catch(AuthenticationException ae){
            throw new BadCredentialsException("Bad Credentials");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return false;
    }
}
