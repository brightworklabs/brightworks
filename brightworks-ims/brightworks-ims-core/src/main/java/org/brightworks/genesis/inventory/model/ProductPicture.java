package org.brightworks.genesis.inventory.model;

import org.brightworks.genesis.commons.model.JpaModel;

import javax.persistence.*;

/**
 * Created by KyelDavid on 1/11/2015.
 */
@Entity
@Table(name = "TXN_PRODUCT_PICTURE")
public class ProductPicture extends JpaModel{

    @Column
    private byte[] picture;

    public ProductPicture(){

    }

    @Transient
    private Boolean empty;

    public Boolean getEmpty() {
        if(picture == null){
            return true;
        }else{
            return  false;
        }
    }

    public void setEmpty(Boolean empty) {
        this.empty = empty;
    }

    public ProductPicture(byte[] image){
        this.picture = image;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

}
