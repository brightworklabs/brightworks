package org.brightworks.genesis.client.controller.product;

import org.brightworks.genesis.inventory.dto.PictureDTO;
import org.brightworks.genesis.inventory.service.ProductPictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by KyelDavid on 1/17/2015.
 */
@Controller
@RequestMapping("/product/image")
public class ProductImageController {

    @Autowired
    private ProductPictureService productPictureService;

    @RequestMapping(value = "/{id}")
    public ResponseEntity<byte[]> fetchImage(@PathVariable("id") Long imageId){
        PictureDTO pictureDTO = productPictureService.findById(imageId);
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        return  new ResponseEntity<byte[]>(pictureDTO.getPicture(),headers,HttpStatus.OK);
    }

}
