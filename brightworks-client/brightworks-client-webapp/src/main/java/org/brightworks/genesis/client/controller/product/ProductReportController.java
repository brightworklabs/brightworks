package org.brightworks.genesis.client.controller.product;

import org.brightworks.genesis.commons.security.SecurityContextService;
import org.brightworks.genesis.commons.security.TenantUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.sql.DataSource;

import static org.brightworks.genesis.client.util.RenderMavBuilder.render;
/**
 * Created by kyeljohndavid on 4/25/2015.
 */
@Controller
@RequestMapping("/product/reports")
public class ProductReportController {

    @Autowired
    @Qualifier("clientDataSource")
    private DataSource dataSource;

    @Autowired
    private SecurityContextService securityContextService;

    @RequestMapping(value = "/salesSummary",method = RequestMethod.GET)
    public ModelAndView  generateSalesSummary(){
        return render("rprt_sales")
            .addAttr("format","pdf")
            .addAttr("tenant",securityContextService.getCurrentTenant())
            .addAttr("datasource",dataSource)
            .toMav();

    }
}
