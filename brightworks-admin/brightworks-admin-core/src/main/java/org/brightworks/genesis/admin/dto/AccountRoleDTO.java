package org.brightworks.genesis.admin.dto;

/**
 * Created by kyeljohndavid on 4/3/2015.
 */
public class AccountRoleDTO {

    private Long id;

    private String desc;

    private String role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
