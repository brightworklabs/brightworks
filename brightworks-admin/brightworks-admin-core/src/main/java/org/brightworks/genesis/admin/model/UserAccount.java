package org.brightworks.genesis.admin.model;

import org.brightworks.genesis.admin.role.AccountRole;
import org.brightworks.genesis.commons.model.BaseAccountModel;

import javax.persistence.*;

/**
 * @author by kyeljmd
 */
@Table(name="USER_ACCOUNT")
@Entity
public class UserAccount extends BaseAccountModel {

    @Column(name = "ROLE")
    @Enumerated(EnumType.STRING)
    private AccountRole role;

    @OneToOne
    @JoinColumn(name ="TENANT_INFO_ID")
    private  TenantInfo tenantInfo;

    public UserAccount(){
        super.setName(new Name());
    }

    public String getTenantCode() {
        return tenantInfo.getTenantCode();
    }

    public AccountRole getRole() {
        return role;
    }

    public void setRole(AccountRole role) {
        this.role = role;
    }

    public TenantInfo getTenantInfo() {
        return tenantInfo;
    }

    public void setTenantInfo(TenantInfo tenantInfo) {
        this.tenantInfo = tenantInfo;
    }
}
