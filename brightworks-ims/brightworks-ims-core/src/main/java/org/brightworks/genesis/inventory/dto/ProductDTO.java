package org.brightworks.genesis.inventory.dto;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * Created by kyel on 12/22/2014.
 */
public class ProductDTO {

    private Long id;

    private String code;

    private String name;

    private String manufacturer;

    private BigDecimal dp;

    private BigDecimal srp;

    private BigDecimal cost;

    private String producType;

    private String description;

    private boolean noImage;
    /**
     * Product picture will be null if has this dto will be sent on the view layer
     * different method will handle the picture display
     */
    private byte[] picture;

    private String pictureContentType;

    private Long pictureId;

    private int stock;

    private String location;

    /**
     * Initialize Default Values for BigDecimal Values
     */
    public ProductDTO(){
        dp = BigDecimal.ZERO;
        cost = BigDecimal.ZERO;
        srp = BigDecimal.ZERO;
    }

    public ProductDTO(Long id, String code, String name, String manufacturer, BigDecimal dp, BigDecimal srp, BigDecimal cost, int stock) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.manufacturer = manufacturer;
        this.dp = dp;
        this.srp = srp;
        this.cost = cost;
        this.stock = stock;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public Long getPictureId() {
        return pictureId;
    }

    public void setPictureId(Long pictureId) {
        this.pictureId = pictureId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public BigDecimal getDp() {
        return dp;
    }

    public void setDp(BigDecimal dp) {
        this.dp = dp;
    }

    public BigDecimal getSrp() {
        return srp;
    }

    public void setSrp(BigDecimal srp) {
        this.srp = srp;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getProducType() {
        return producType;
    }

    public void setProducType(String producType) {
        this.producType = producType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getPictureContentType() {
        return pictureContentType;
    }

    public void setPictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Boolean isAvailable(){
        if(stock !=0)
            return true;
        else
            return false;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    public boolean isNoImage() {
        return noImage;
    }

    public void setNoImage(boolean noImage) {
        this.noImage = noImage;
    }

    public boolean isNew(){
        return (id == null);
    }


    @Override
    public String toString() {
        return "ProductDTO{" +
            "id=" + id +
            ", code='" + code + '\'' +
            ", name='" + name + '\'' +
            ", manufacturer='" + manufacturer + '\'' +
            ", dp=" + dp +
            ", srp=" + srp +
            ", cost=" + cost +
            ", producType='" + producType + '\'' +
            ", description='" + description + '\'' +
            ", noImage=" + noImage +
            ", picture=" + Arrays.toString(picture) +
            ", pictureContentType='" + pictureContentType + '\'' +
            ", pictureId=" + pictureId +
            ", stock=" + stock +
            ", location='" + location + '\'' +
            '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductDTO)) return false;

        ProductDTO that = (ProductDTO) o;

        if (!code.equals(that.code)) return false;
        if (!id.equals(that.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + code.hashCode();
        return result;
    }
}
