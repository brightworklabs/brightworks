package org.brightworks.genesis.inventory.repo;

import org.brightworks.genesis.inventory.model.InventoryItem;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kyel on 12/22/2014.
 */
public interface InventoryItemRepo extends JpaRepository<InventoryItem,Long> {
}
