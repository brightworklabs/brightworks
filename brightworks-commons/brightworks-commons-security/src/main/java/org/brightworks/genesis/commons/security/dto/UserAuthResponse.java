package org.brightworks.genesis.commons.security.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kyeljohndavid on 10/29/14.
 */
public class UserAuthResponse {

    private UserAuthDto userAuthDto;

    public UserAuthResponse(UserAuthDto userAuthDto){
        this.userAuthDto = userAuthDto;
    }

    public UserAuthResponse(UserAuthDto userAuthDto,boolean authenticated){
        this.userAuthDto = userAuthDto;
        this.authenticated = authenticated;
    }

    public UserAuthResponse(){

    }

    private boolean authenticated;

    public boolean isAuthenticated() {
        if(userAuthDto == null){
            return false;
        }
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public String getUsername(){
        return userAuthDto != null ? userAuthDto.getUsername() : "";
    }

    public List<String> getRole(){
        return userAuthDto != null ? userAuthDto.getRoles(): new ArrayList<String>();
    }

    public String getTenantCode(){
        return  userAuthDto !=null ? userAuthDto.getTenantCode() : "";
    }

    public UserAuthDto getUserAuthDto() {
        return userAuthDto;
    }

    public void setUserAuthDto(UserAuthDto userAuthDto) {
        this.userAuthDto = userAuthDto;
    }
}
