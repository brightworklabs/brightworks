package org.brightworks.genesis.inventory.mapper;

import org.brightworks.genesis.inventory.dto.LineItemDTO;
import org.brightworks.genesis.inventory.model.transaction.LineItem;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * Created by kyeljohndavid on 3/28/2015.
 */

@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface LineItemMapper {


    LineItemMapper INSTANCE = Mappers.getMapper(LineItemMapper.class);

    @Mappings(value = {
        @Mapping(source = "srp", target = "srp"),
        @Mapping(source = "dp", target = "dp"),
        @Mapping(source = "cost", target = "cost"),
        @Mapping(source = "cancelled", target = "cancelled"),
        @Mapping(source = "qty",target = "qty" ),
        @Mapping(source = "total",target = "total" ),
        @Mapping(source = "priceType",target = "priceType" ),
        @Mapping(source = "itemCode", target = "itemCode"),
        @Mapping(source = "itemName", target = "itemName"),
        @Mapping(source = "refunded", target = "refunded")
    })
    LineItemDTO toLineItemDto(LineItem item);

    List<LineItemDTO> toLineItemDtos(List<LineItem> lineItemDTOs);
}
