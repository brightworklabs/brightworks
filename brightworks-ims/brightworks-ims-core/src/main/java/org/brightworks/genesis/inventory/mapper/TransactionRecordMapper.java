package org.brightworks.genesis.inventory.mapper;

import org.brightworks.genesis.inventory.dto.TransactionRecordDTO;
import org.brightworks.genesis.inventory.model.transaction.TransactionRecord;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.factory.Mappers;

/**
 * Created by kyeljohndavid on 3/27/2015.
 */

@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface TransactionRecordMapper  {


    TransactionRecordMapper INSTANCE = Mappers.getMapper(TransactionRecordMapper.class);

    @Mappings(value = {
        @Mapping(source = "id", target = "transactionId"),
        @Mapping(source = "transactionNumber", target = "transactionNumber"),
        @Mapping(source = "transactionDate", target = "date"),
        @Mapping(source = "lineItems",target = "lineItems",ignore = true),
        @Mapping(source = "valid",target = "valid"),
        @Mapping(source = "total",target = "totalAmount")
    })
    TransactionRecordDTO toTransactionRecord(TransactionRecord txnRecord);
}
