package org.brightworks.genesis.inventory.service;

import org.brightworks.genesis.inventory.dto.PictureDTO;

/**
 * Created by KyelDavid on 1/17/2015.
 */
public interface ProductPictureService {

    PictureDTO findById(Long id);
}
