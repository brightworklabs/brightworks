package org.brightworks.genesis.commons.security;

import org.brightworks.genesis.commons.security.dto.UserAuthDto;
import org.brightworks.genesis.commons.security.dto.UserAuthResponse;
import org.springframework.security.core.AuthenticationException;

/**
 * Created by kyel on 11/3/2014.
 *
 * It is up to the client app(e.g client webapp) to build the Tenant Authentication
 * mechanism
 */
public interface TenantAccountAuthService {

    /**
     *  TODO: Return User information DTO
     * @param authDto
     * @throws AuthenticationException
     */
    UserAuthResponse authenticate(UserAuthDto authDto) throws AuthenticationException;
}
