package org.brightworks.genesis.admin.forms;


import org.brightworks.genesis.admin.dto.TenantInfoDTO;

/**
 * Created by kyeljohndavid on 4/3/2015.
 */
public class TenantForm {

    private TenantInfoDTO tenant;

    public TenantForm(){
        tenant = new TenantInfoDTO();
    }

    public TenantForm(TenantInfoDTO tenant) {
        this.tenant = tenant;
    }

    public TenantInfoDTO getTenant() {
        return tenant;
    }

    public void setTenant(TenantInfoDTO tenant) {
        this.tenant = tenant;
    }

    public Long getId() {
        return tenant.getId();
    }

    public void setId(Long id) {
        tenant.setId(id);
    }

    public String getCode() {
        return tenant.getTenantCode();
    }

    public void setCode(String tenantCode) {
        tenant.setTenantCode(tenantCode);
    }

    public String getDescription() {
        return tenant.getTenantDescription();
    }

    public void setDescription(String tenantDesc) {
        tenant.setTenantDescription(tenantDesc);
    }
}
