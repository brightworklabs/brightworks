package org.brightworks.genesis.inventory.repo;

import org.brightworks.genesis.inventory.model.transaction.LineItem;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kyeljohndavid on 3/26/2015.
 */
public interface LineItemRepo extends JpaRepository<LineItem,Long> {
}
