<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Brightworks Kurt">
    <title>Kenlex Telecoms | Login</title>
    <!-- Bootstrap Core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/login-page.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="${pageContext.request.contextPath}/resources/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Page Content -->
<div class="container">
    <div class="content centered" id="login-page-content">

        <div class="brand">
            <h3>Branch and User Management</h3>
        </div>


        <c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
            <div class="alert" id="alert-message">${SPRING_SECURITY_LAST_EXCEPTION.message}</div>
        </c:if>
        <form class="form-signin"name="f" action="/j_spring_security_check" method="POST">
            <fieldset>

                <!-- Username -->
                <div class="input-field-container">
					<input class="input-field" type="text" id="input-1" name="j_username" placeholder="Username"/>
				</div>

                <!-- Password -->
                <div class="input-field-container">
					<input class="input-field" type="password" id="input-2" name="j_password" placeholder="Password"/>
				</div>

                <!-- Login Button -->
                <div class="btn-container">
                    <button name="submit" class="btn btn-block" type="submit">Login</button>
                </div>

            </fieldset>
        </form>
    </div>
</div>
<!-- /.container -->
<!-- jQuery Version 1.11.0 -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

<!-- Login Page JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
</body>
</html>
