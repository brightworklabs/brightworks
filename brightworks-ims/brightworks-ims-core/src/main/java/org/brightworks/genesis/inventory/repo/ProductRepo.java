package org.brightworks.genesis.inventory.repo;

import org.brightworks.genesis.inventory.model.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by kyel on 12/22/2014.
 */
public interface ProductRepo extends JpaRepository<Product,Long>, ProductRepoCustom {

    List<Product> findByManufacturer(String manufacturer);

    Product findByCode(String itemCode);

    Product findByCodeIgnoreCase(String itemCode);

    List<Product> findProductsByCode(String itemCode);

    List<Product> findProductsByName(String name);

    List<Product> findProductByNameOrCode(String name, String code);

    List<Product> findByNameLike(String name,Pageable pageable);
}
