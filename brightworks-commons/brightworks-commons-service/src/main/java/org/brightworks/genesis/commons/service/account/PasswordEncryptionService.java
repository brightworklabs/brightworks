package org.brightworks.genesis.commons.service.account;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Created by kyel on 10/5/2014.
 */
public interface PasswordEncryptionService extends PasswordEncoder {

    String encode(CharSequence rawPassword);

    boolean matches(CharSequence rawPassword,String  encodedPassword);
}
