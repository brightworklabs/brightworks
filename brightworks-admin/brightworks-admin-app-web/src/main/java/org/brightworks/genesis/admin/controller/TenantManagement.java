package org.brightworks.genesis.admin.controller;

import org.brightworks.genesis.admin.forms.UserAccountForm;
import org.brightworks.genesis.admin.repo.TenantInfoRepo;
import org.brightworks.genesis.admin.role.AccountRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;

import static org.brightworks.genesis.admin.util.MavBuilder.render;

/**
 * Created by kyel on 3/5/2016.
 */
@Controller
@RequestMapping("/branch")
public class TenantManagement {

    private final String ALL_TENANTS_PAGE = "tenantList";

    @Autowired
    private TenantInfoRepo tenantInfoRepo;

    @RequestMapping("/all")
    public ModelAndView viewAllTenants() {
        return render(ALL_TENANTS_PAGE)
                .addAttr("tenants",tenantInfoRepo.findAll())
                .toMav();

    }
}