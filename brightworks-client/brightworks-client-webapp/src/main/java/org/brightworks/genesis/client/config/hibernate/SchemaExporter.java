package org.brightworks.genesis.client.config.hibernate;

import org.brightworks.genesis.commons.model.JpaModel;
import org.brightworks.genesis.inventory.model.Product;
import org.brightworks.genesis.inventory.model.ProductPicture;
import org.brightworks.genesis.inventory.model.transaction.LineItem;
import org.brightworks.genesis.inventory.model.transaction.TransactionRecord;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.Target;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Created by kyel on 12/6/2014.
 */
public class SchemaExporter {

    private static List<Class<? extends JpaModel>> inventoryModelClass =
            Arrays.asList(TransactionRecord.class,LineItem.class,
            Product.class, ProductPicture.class);

    public static void main(String[] args){

        Configuration config = new Configuration();

        Properties properties = new Properties();

        properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        properties.put("hibernate.connection.url", "jdbc:postgresql://localhost:5432/genesis_cms");
        properties.put("hibernate.connection.username", "postgres");
        properties.put("hibernate.connection.password", "postgres");
        properties.put("hibernate.connection.driver_class", "org.postgresql.Driver");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "update");
        config.setProperties(properties);

        /**
         * We place here all the annotated classes
         * That we'll be used by the content management system(Inventory,
         * and sales management system)
         */

        for(Class clazz: inventoryModelClass){
            config.addAnnotatedClass(clazz);
        }


        SchemaExport schemaExport = new SchemaExport(config);
        schemaExport.setDelimiter(";");
        schemaExport.setOutputFile("import.sql");
        /**Just dump the schema SQLs to the console , but not execute them ***/
        //schemaExport.create(true, false);
        schemaExport.create(Target.EXPORT);

    }
}
