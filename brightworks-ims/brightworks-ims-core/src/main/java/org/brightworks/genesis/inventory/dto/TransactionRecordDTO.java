package org.brightworks.genesis.inventory.dto;

import org.brightworks.genesis.inventory.mapper.LineItemMapper;
import org.brightworks.genesis.inventory.model.transaction.LineItem;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by kyeljohndavid on 3/24/2015.
 */
public class TransactionRecordDTO {

    private Long transactionId;

    private String transactionNumber;

    private DateTime date;

    private List<LineItemDTO> lineItems;

    private  boolean valid;

    private BigDecimal totalAmount;

    public TransactionRecordDTO(){}

    public TransactionRecordDTO(Long transactionId,
                                String transactionNumber,
                                DateTime date,
                                boolean valid, BigDecimal totalAmount,
                                List<LineItem> items) {
        this.transactionId = transactionId;
        this.transactionNumber = transactionNumber;
        this.date = date;
        this.valid = valid;
        this.totalAmount = totalAmount;
        this.lineItems = LineItemMapper.INSTANCE.toLineItemDtos(items);

    }


    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public List<LineItemDTO> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItemDTO> lineItems) {
        this.lineItems = lineItems;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getFormattedDate(){
        DateTimeFormatter  format = DateTimeFormat.forPattern("MM/dd/yyyy");
        return format.print(date);
    }


}
