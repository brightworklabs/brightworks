package org.brightworks.genesis.admin.repo;

import org.brightworks.genesis.admin.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by kyel on 10/20/2014.
 */
@Repository
public interface UserAccountRepo extends JpaRepository<UserAccount,Long>, UserAccountRepoCustom {

    UserAccount findByUsername(String username);

    List<UserAccount> findByTenantInfoTenantCode(String tenantCode);
}
