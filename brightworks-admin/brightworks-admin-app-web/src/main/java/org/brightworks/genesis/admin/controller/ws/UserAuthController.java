package org.brightworks.genesis.admin.controller.ws;


import org.brightworks.genesis.admin.service.TenantDetailsService;
import org.brightworks.genesis.commons.security.TenantUserDetails;
import org.brightworks.genesis.commons.security.dto.UserAuthDto;
import org.brightworks.genesis.commons.security.dto.UserAuthResponse;
import org.brightworks.genesis.commons.service.account.PasswordEncryptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author  kyel
 *
 * Internal REST API that authenticates
 * Tenant accounts from different clients
 */
@RestController
@RequestMapping("/api/account/")
public class UserAuthController {

    @Autowired
    private TenantDetailsService tenantService;

    @Autowired
    private PasswordEncryptionService passwordEncryptionService;

    @RequestMapping(value = "authenticate",
            method = RequestMethod.POST,consumes = "application/json",
            produces = "application/json")
    public UserAuthResponse authenticate(@RequestBody UserAuthDto userAuth){
         try{

             TenantUserDetails userDetails = (TenantUserDetails) tenantService.
                    loadUserByUsername(userAuth.getUsername());
            if(passwordEncryptionService.matches(userAuth.getPassword(),userDetails.getPassword())){
                return  new UserAuthResponse(new UserAuthDto(userDetails.getUsername(),
                        userDetails.getPassword(),userDetails.getRoles(),
                        userDetails.getTenantCode()),true);
            }else{
                return new UserAuthResponse();
            }
        }catch (UsernameNotFoundException unfe){
            return new UserAuthResponse();
        }
    }
}
