package org.brightworks.genesis.admin.service.impl;

import org.brightworks.genesis.admin.dto.TenantInfoDTO;
import org.brightworks.genesis.admin.mapper.TenantInfoMapper;
import org.brightworks.genesis.admin.model.TenantInfo;
import org.brightworks.genesis.admin.repo.TenantInfoRepo;
import org.brightworks.genesis.admin.service.TenantInfoService;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by kyeljohndavid on 4/17/2015.
 */
@Service
@Transactional
public class TenantInfoServiceImpl implements TenantInfoService{

    @Autowired
    private TenantInfoRepo tenantInfoRepo;

    @Autowired
    @Qualifier("clientDataSource")
    private DataSource clientDataSource;


    /**
     *
     * @param tenantCode of the requested Branch/Tenant
     * @return TenantInfo that's saved on the Database
     */
    @Override
    public TenantInfoDTO getTenantInfo(String tenantCode){
        return TenantInfoMapper.INSTANCE.toDto(tenantInfoRepo.findByTenantCode(tenantCode));
    }

    /**
     *
     * @param tenantInfo THe tenant information to be saved on the database
     * @return TenantInfo that's has been saved on the database.
     */
    @Override
    public TenantInfoDTO save(TenantInfoDTO tenantInfo) {
        TenantInfo tenant = buildTenantInfo(tenantInfo);
        tenantInfoRepo.save(tenant);
        createSchema(tenantInfo.getTenantCode());
        return tenantInfo;
    }

    private TenantInfo buildTenantInfo(TenantInfoDTO tenantInfo) {
        TenantInfo tenant = new TenantInfo();
        tenant.setTenantCode(tenantInfo.getTenantCode());
        tenant.setTenantDescription(tenantInfo.getTenantDescription());
        return tenant;
    }

    /**
     *
     * @param code - The tenant code that is used for searching a tenant
     * @return true or false if the tenant exists
     */
    @Override
    public boolean exists(String code) {
        TenantInfo tenantInfo  = tenantInfoRepo.findByTenantCode(code);
        return (tenantInfo != null);
    }

    /**
     *
     * @return All existing Tenants/Branches
     */
    @Override
    public List<TenantInfoDTO> getAll(){
        return TenantInfoMapper.INSTANCE.toTenantInfoDtos(tenantInfoRepo.findAll());
    }

    /**
     * @param schemaName Schema name to be created on the client database
     */
    private void createSchema(String schemaName){
        try{
            Flyway flyway = new Flyway();
            flyway.setDataSource(clientDataSource);
            flyway.setSchemas(schemaName);
            if(schemaName.equals("admin")){
                flyway.setLocations("classpath:/manual-sql-scripts/");
            }else{
                flyway.setLocations("classpath:/tenant-scripts/");
            }
            flyway.migrate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
