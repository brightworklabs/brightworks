package org.brightworks.genesis.inventory.repo;

import org.brightworks.genesis.inventory.model.transaction.TransactionRecord;
import org.joda.time.DateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * Created by kyeljohndavid on 6/21/2015.
 */
public interface TransactionRecordRepoCustom {

    Page<TransactionRecord> filterByDateRange(DateTime from, DateTime to, Pageable page);
}
