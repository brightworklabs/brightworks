package org.brightworks.genesis.admin.service.impl;

import org.brightworks.genesis.admin.dto.UserAccountDTO;
import org.brightworks.genesis.admin.mapper.UserAccountMapper;
import org.brightworks.genesis.admin.model.TenantInfo;
import org.brightworks.genesis.admin.model.UserAccount;
import org.brightworks.genesis.admin.repo.TenantInfoRepo;
import org.brightworks.genesis.admin.repo.UserAccountRepo;
import org.brightworks.genesis.admin.service.UserAccountService;
import org.brightworks.genesis.commons.service.account.PasswordEncryptionService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author  by kyel on 10/5/2014.
 */
@Service
@Transactional
public class AccountServiceImpl implements UserAccountService {

    @Autowired
    private UserAccountRepo userAccountRepo;

    @Autowired
    private TenantInfoRepo tenantInfoRepo;

    @Autowired
    private PasswordEncryptionService passwordEncryptionService;

    /**
     *
     * @param accountDTO DTO that contains the object to be persisted
     * @return Returns the successfully saved account
     */
    public UserAccountDTO saveAccountInformation(UserAccountDTO accountDTO) {
        if(accountDTO.getId() != null){
            UserAccount userAccount = userAccountRepo.findOne(accountDTO.getId());
            userAccount.setTenantInfo(tenantInfoRepo.findByTenantCode(accountDTO.getTenantCode()));
            userAccount.setGivenName(accountDTO.getFirstName());
            userAccount.setLastName(accountDTO.getLastName());
            userAccount.setRole(accountDTO.getAccountRole());
            userAccountRepo.save(userAccount);
        }else {
            String encodedPassword = passwordEncryptionService.encode(accountDTO.getPassword());
            TenantInfo tenant =  tenantInfoRepo.findByTenantCode(accountDTO.getTenantCode());
            UserAccount userAccount = buildUserAccount(accountDTO, encodedPassword, tenant);
            UserAccount savedUserAccount = userAccountRepo.save(userAccount);
            accountDTO.setId(savedUserAccount.getId());
        }

        return  accountDTO;
    }

    private UserAccount buildUserAccount(UserAccountDTO accountDTO, String encodedPassword, TenantInfo tenant) {
        UserAccount userAccount = new UserAccount();
        userAccount.setUsername(accountDTO.getUsername());
        userAccount.setPassword(encodedPassword);
        userAccount.setRole(accountDTO.getAccountRole());
        userAccount.setTenantInfo(tenant);
        userAccount.setGivenName(accountDTO.getFirstName());
        userAccount.setLastName(accountDTO.getLastName());
        userAccount.setCreatedDate(new DateTime());
        userAccount.setActive(true);
        return userAccount;
    }

    /**
     * @param username of account to be searched
     * @return UserAccount, returns null if not existing
     */
    @Override
    public UserAccount findByUsername(String username) {
        return userAccountRepo.findByUsername(username);
    }


    /**
     * @param username of account to be checked if existing
     * @return true if the account is existing,otherwise false
     */
    @Override
    public boolean exists(String username) {
      return (findByUsername(username) != null);
    }

    /**
     * @param id if account to be searched
     * @return UserAccount
     */
    @Override
    public UserAccountDTO findById(Long id) {
        return UserAccountMapper.INSTANCE.toDto(userAccountRepo.findOne(id));
    }

    /**
     *
     * @param term this could either be a last name or pin
     * @param pageable Pageable instance that contains the page request
     * @return Page of UserAccountDTOS
     */
    @Override
    public Page<UserAccountDTO> search(String term, Pageable pageable) {
        return userAccountRepo.search(term,pageable);
    }

    /**
     * @param code is the branch code
     * @return List of userAccounts registered on the given tenant code
     */
    @Override
    public List<UserAccountDTO> findByBranchCode(String code) {
        return UserAccountMapper.INSTANCE.toUserAcountDtos(userAccountRepo.findByTenantInfoTenantCode(code));
    }
}
