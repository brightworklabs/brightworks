package org.brightworks.genesis.inventory.dto;

/**
 * Created by kyeljohndavid on 5/17/2015.
 */
public class TenantInfoResponse {

    private Long id;

    private String tenantCode;

    private String tenantDescription;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getTenantDescription() {
        return tenantDescription;
    }

    public void setTenantDescription(String tenantDescription) {
        this.tenantDescription = tenantDescription;
    }

    public boolean isNew(){
        return (id == null);
    }

}
