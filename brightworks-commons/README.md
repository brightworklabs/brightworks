BrightWorks Commons Library
===========================

Contains all the bare bones dependencies needed by
all modules.

REMEMBER: The contents of this module should not be dependent
on the client.

We decomposed the modules by their layer rather than
by module/feature so that we can easily reuse modules

brightworks-commons-model
-Contains all the Models to be that will be used all modules
that will be persisted(e.g Save in the Database)


brightworks-commons-service
-Contains Abstraction of various logic that will be used
by various modules.