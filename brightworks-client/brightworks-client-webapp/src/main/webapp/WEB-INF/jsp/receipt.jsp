<%--
  Created by IntelliJ IDEA.
  User: Kurt
  Date: 3/18/2015
  Time: 5:11 PM
  To change this template use File | Settings | File Templates.
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<jsp:include page="./includes/head.jsp" flush="true"/>
<body>
<!-- Header -->
<jsp:include page="./includes/page-header.jsp" flush="true" />
<!-- Content-->
<div class="container-fluid" id="container">
    <!-- Sidebar -->
    <jsp:include page="./includes/side-bar.jsp" flush="true" />
    <!-- Page content -->
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            Transaction Summary
        </div>

        <div class="panel-body">
            <div class="col-xs-12 col-md-8 col-lg-8 centered">
                <div class="row">
                    <div class="transaction-number text-center">
                        <strong class="text-primary">Transaction Number</strong>
                        <h4>${receipt.transactionNumber}</h4>
                        <h4>${receipt.formattedDate}</h4>
                    </div>
                </div><!--/.row-->

                <hr>

                <div class="row">
                    <div class="col-xs-12 col-md-5 col-md-offset-7 no-padding">
                        <div class="col-xs-4 text-right">
                            <div class="receipt-page-bg-text text-primary">Total</div>
                        </div>
                        <div class="col-xs-8 text-right no-padding">
                            <div class="receipt-page-bg-text" id="total">PHP ${receipt.totalAmount}</div>
                        </div>
                    </div>
                </div><!--/.row-->

                <hr>

                <div class="row">
                    <table class="table table-striped receipt-table">
                        <!-- Table Headers -->
                        <thead>
                            <tr class="text-primary">
                                <th>Item Name</th>
                                <th>Quantity</th>
                                <th>Price</th>
                            </tr>
                        </thead>

                        <!-- Table Body -->
                        <tbody>
                            <c:forEach items="${receipt.lineItems}" var="item" varStatus="status">
                                <tr>
                                    <td>${item.itemName}</td>
                                    <td>${item.qty}</td>
                                    <td><span>PHP </span>${item.total}</td>
                                    </tr>
                            </c:forEach>
                        </tbody>

                    </table>
                </div><!--/.row-->

                <hr>

                <div class="row">
                    <div class="back-to-checkout pull-right">
                        <a href="${pageContext.request.contextPath}/product/checkout" class="btn btn-primary" id="confirmButton" role="button">
                            New Transaction
                            <i class="fa fa-pencil-square-o fa-sm"></i>
                        </a>
                    </div>

                    <div class="back-to-dashboard pull-right">
                        <a href="${pageContext.request.contextPath}/product/search" class="btn btn-default" id="confirmButton" role="button">
                            Go back to Dashboard
                        </a>
                    </div>
                </div><!--/.row-->

            </div><!--/.centered-->
        </div><!--/.panel-body-->
    </div><!--/.panel-->

    <!-- Modal -->
    <div class="modal fade" id="confirmModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal</h4>
                </div>

                <div class="modal-body">
                    test text.
                </div>
            </div>
        </div>
    </div><!--/.modal-->

</div><!--/.container-->
<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.11.0.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- Login Page JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
</body>
</html>
