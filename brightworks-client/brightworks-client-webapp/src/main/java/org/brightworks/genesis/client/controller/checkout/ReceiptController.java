package org.brightworks.genesis.client.controller.checkout;

import org.brightworks.genesis.client.util.MavBuilder;
import org.brightworks.genesis.inventory.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Kurt on 3/19/2015.
 */

@Controller
public class ReceiptController {

    private Logger logger = LoggerFactory.getLogger(ReceiptController.class);

    private static String RECEIPT = "receipt";

    private static String RECEIPT_PAGE = "receipt";

    private static String RECEIPT_SEARCH_PAGE = "receipt/searchReceipts";

    @Autowired
    private TransactionService transactionService;

    @RequestMapping(value="/receipt/{transactionId}")
    public ModelAndView displayDashboard(@PathVariable("transactionId") String number) {
        return MavBuilder
            .render(RECEIPT_PAGE)
            .addAttr(RECEIPT,transactionService.findTransactionRecord(number))
            .toMav();
    }

    @RequestMapping(value="/receipt/search")
    public String displayReceiptsSearch() {
        return RECEIPT_SEARCH_PAGE;
    }
}
