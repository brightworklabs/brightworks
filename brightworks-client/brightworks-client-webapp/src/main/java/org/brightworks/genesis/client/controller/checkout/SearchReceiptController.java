package org.brightworks.genesis.client.controller.checkout;

import org.brightworks.genesis.client.controller.datatables.DataTablesResponse;
import org.brightworks.genesis.inventory.dto.TransactionRecordDTO;
import org.brightworks.genesis.inventory.service.TransactionService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by kyeljohndavid on 6/24/2015.
 */
@RestController
public class SearchReceiptController {

    @Autowired
    private TransactionService transactionService;

    @RequestMapping(value = "/receipt/ajax/search",method = RequestMethod.GET)
    public DataTablesResponse<TransactionRecordDTO> dataTableSearchProduct(
        @RequestParam(required = false) Object data ,@RequestParam(required=true) int sEcho,
        @RequestParam (required=true) int iDisplayStart,
        @RequestParam (required=true) int iDisplayLength,
        @RequestParam (required=true) int iColumns,
        @RequestParam (required=true) int iSortCol_0,
        @RequestParam (required=false)String sSortDir_0,
        @RequestParam (required=true) String sSearch,
        @RequestParam(required = false) String fromDate,
        @RequestParam(required = false) String toDate){


        int page = (int) Math.ceil(iDisplayStart/iDisplayLength);

        DateTime fromDateTime = null;
        DateTime toDateTime = null;
        if(fromDate != null && !fromDate.isEmpty()){
            fromDateTime = new DateTime(fromDate);
        }

        if(toDate != null && !toDate.isEmpty()){
            toDateTime = new DateTime(toDate);
        }
        Page<TransactionRecordDTO> productPage = transactionService
            .filterByDateRange(fromDateTime, toDateTime, new PageRequest(page, iDisplayLength));

        return new DataTablesResponse<>(productPage.getContent(),
            sEcho,
            productPage.getTotalElements(),
            productPage.getTotalElements());
    }
}
