
    create table REF_PRODUCT (
        id int8 not null,
        created_date timestamp,
        CODE varchar(255),
        DESCRIPTION varchar(255),
        LOCATION varchar(255),
        MANUFACTURER varchar(255),
        NAME varchar(255),
        COST numeric(19, 2),
        DEALERS_PRICE numeric(19, 2),
        SUGGESTED_RETAIL_PRICE numeric(19, 2),
        STOCK int4,
        PRODUCT_TYPE varchar(255),
        picture_id int8,
        primary key (id)
    );

    create table TXN_LINE_ITEM (
        id int8 not null,
        created_date timestamp,
        cancelled boolean,
        cost numeric(19, 2),
        dp numeric(19, 2),
        itemCode varchar(255),
        itemName varchar(255),
        priceType varchar(255),
        qty int4,
        refunded boolean,
        srp numeric(19, 2),
        total numeric(19, 2),
        transactionRecord_id int8,
        transaction_date timestamp,
        primary key (id)
    ) INHERITS (admin.TXN_LINE_ITEM);

    create table TXN_PRODUCT_PICTURE (
        id int8 not null,
        created_date timestamp,
        picture bytea,
        primary key (id)
    );

    create table TXN_TRANSACTION_RECORD (
        id int8 not null,
        created_date timestamp,
        total numeric(19, 2),
        transaction_date timestamp,
        TRANSACTION_NUMBER varchar(255),
        VALID boolean,
        primary key (id)
    ) INHERITS (admin.TXN_TRANSACTION_RECORD);

    alter table REF_PRODUCT
        add constraint FK_sjugahpelk16qj5h3w8dli42l
        foreign key (picture_id)
        references TXN_PRODUCT_PICTURE;

    alter table TXN_LINE_ITEM
        add constraint FK_o5mslaahpil9d3g9rl2s22rpm
        foreign key (transactionRecord_id)
        references TXN_TRANSACTION_RECORD;

    create table SEQ_ENTITY_ID (
         sequence_name varchar(255),
         sequence_next_hi_value int4
    );



INSERT INTO SEQ_ENTITY_ID(
        SELECT *
        FROM admin.SEQ_ENTITY_ID
);

INSERT INTO TXN_PRODUCT_PICTURE
   (
      SELECT *
        FROM admin.TXN_PRODUCT_PICTURE
   );

INSERT INTO REF_PRODUCT
   (
      SELECT *
        FROM admin.REF_PRODUCT
   );
