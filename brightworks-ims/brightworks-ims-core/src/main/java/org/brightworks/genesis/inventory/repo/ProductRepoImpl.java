package org.brightworks.genesis.inventory.repo;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import org.brightworks.genesis.inventory.dto.ProductDTO;
import org.brightworks.genesis.inventory.model.QProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by kyeljohndavid on 3/31/2015.
 */
public class ProductRepoImpl implements ProductRepoCustom {

    @Autowired
    private EntityManager em;

    @Override
    public Page<ProductDTO> search(String term, Pageable pageable) {
        JPAQuery query = new JPAQuery(em);
        QProduct product = QProduct.product;

        List<ProductDTO> productList =
            query.from(product).where(product.code.eq(term).or(product.name.containsIgnoreCase(term)))
                .limit(pageable.getPageSize())
                .offset(pageable.getOffset())
                .list(ConstructorExpression.create(ProductDTO.class,
                    product.id,
                    product.code,
                    product.name,
                    product.manufacturer,
                    product.price.dp,
                    product.price.srp,
                    product.price.cost,
                    product.stock));


        return new PageImpl<ProductDTO>(productList,pageable,query.count());
    }
}
