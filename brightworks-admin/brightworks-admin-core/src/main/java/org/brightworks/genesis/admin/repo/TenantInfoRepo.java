package org.brightworks.genesis.admin.repo;

import org.brightworks.genesis.admin.model.TenantInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by kyeljohndavid on 4/17/2015.
 */
@Repository
public interface TenantInfoRepo extends JpaRepository<TenantInfo,Long> {
    TenantInfo findByTenantCode(String tenantCode);
}
