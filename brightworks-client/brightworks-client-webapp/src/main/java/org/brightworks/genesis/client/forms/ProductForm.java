package org.brightworks.genesis.client.forms;

import org.brightworks.genesis.inventory.dto.ProductDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by kyel on 12/23/2014.
 */
public class ProductForm {

    private MultipartFile productImage;

    private ProductDTO productDTO;

    public ProductForm(){
        productDTO = new ProductDTO();
    }

    public ProductForm(ProductDTO productDTO){
        this.productDTO = productDTO;
    }

    public String getCode() {
        return productDTO.getCode();
    }

    public void setCode(String code) {
        productDTO.setCode(code);
    }

    public String getName() {
        return productDTO.getName();
    }

    public void setName(String name) {
        productDTO.setName(name);
    }

    public String getManufacturer() {
        return productDTO.getManufacturer();
    }

    public void setManufacturer(String manufacturer) {
        productDTO.setManufacturer(manufacturer);
    }

    public BigDecimal getDp() {
        return productDTO.getDp();
    }

    public void setDp(BigDecimal dp) {
        productDTO.setDp(dp);
    }

    public BigDecimal getSrp() {
        return productDTO.getSrp();
    }

    public void setSrp(BigDecimal srp) {
        productDTO.setSrp(srp);
    }

    public BigDecimal getCost() {
        return productDTO.getCost();
    }

    public void setCost(BigDecimal cost) {
        productDTO.setCost(cost);
    }

    public String getProducType() {
        return productDTO.getProducType();
    }

    public void setProducType(String producType) {
        productDTO.setProducType(producType);
    }

    public Long getId() {
        return productDTO.getId();
    }

    public void setId(Long id) {
        productDTO.setId(id);
    }

    public MultipartFile getProductImage() {
        return productImage;
    }

    public String getLocation(){
        return productDTO.getLocation();
    }

    public void setLocation(String location){
        productDTO.setLocation(location);
    }


    /**
     * If an IOEXception occured, one of the probable cause is failing to open
     * the temporary storage location of the image
     * @return ProductDTO
     * @throws IOException;
     */
    public void setProductImage(MultipartFile productImage) {
        this.productImage = productImage;
        if(this.productImage != null && !this.productImage.isEmpty()) {
            try{
                byte[] imageInBytes = new byte[this.productImage.getBytes().length];
                System.arraycopy(this.productImage.getBytes(),0,imageInBytes,0,this.productImage.getBytes().length);
                productDTO.setPicture(imageInBytes);
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
    }

    public ProductDTO getProductDTO(){
        return productDTO;
    }

    public void setProductDTO(ProductDTO productDTO) {
        this.productDTO = productDTO;
    }

    public String getDescription() {
        return productDTO.getDescription();
    }

    public void setDescription(String description) {
        productDTO.setDescription(description);
    }

    public void setStock(int stock){
        productDTO.setStock(stock);
    }

    public int getStock(){
        return productDTO.getStock();
    }

    @Override
    public String toString() {
        return "ProductForm{" +
                "productDTO=" + productDTO +
                '}';
    }
}
