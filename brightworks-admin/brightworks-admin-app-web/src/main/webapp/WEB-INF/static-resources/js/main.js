/**
 * authors: kurt,kyel
 */
$(function() {
    if ($('#alert-message').text().length > 0) {
        $('#alert-message').slideDown();
    }

});

/**
 * New Sidebar
 * Author: Kurt
 */
$(function() {
    $("#sidebar-toggle").click(function(e) {
        e.preventDefault();
        $("#container").toggleClass("active");
    });
});

/**
 * Image Import And Preview
 * Author: Kurt
 */
$(document).on('click', '#close-preview', function(){
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
            $('.image-preview').popover('show');
        },
        function () {
            $('.image-preview').popover('hide');
        }
    );
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse");
    });
    // Create the preview image
    $(".image-preview-input input:file").change(function (){
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }
        reader.readAsDataURL(file);
    });
});

// Checkout Page Functions

$("#addButton").click(function (e) {
    var addCheckoutRow = "\r\n                <div class=\"well\" id=\"checkout-item\">\r\n                        <fieldset>\r\n                            <div class=\"form-group col-md-2\">\r\n                                <label class=\"control-label\" for=\"itemCode\">\r\n                                    Item Code\r\n                                <\/label>\r\n                                <input type=\"text\" class=\"itemCode form-control\" id=\"itemCode\" placeholder=\"Item Code\">\r\n                            <\/div>\r\n                            <div class=\"form-group col-md-2\">\r\n                                <label class=\"control-label\" for=\"itemName\">\r\n                                    Item Name\r\n                                <\/label>\r\n                                <input type=\"text\" class=\"itemName form-control\" id=\"itemName\" placeholder=\"Item Name\">\r\n                            <\/div>\r\n                            <div class=\"form-group col-md-3\">\r\n                                <label class=\"control-label\" for=\"transactionType\">\r\n                                    Transaction Type\r\n                                <\/label>\r\n                                <select class=\"transactionType form-control\" id=\"transactionType\">\r\n                                    <option>Suggested Retail Price<\/option>\r\n                                    <option>Dealer\'s Price<\/option>\r\n                                <\/select>\r\n                            <\/div>\r\n                            <div class=\"form-group col-md-1 col-xs-6\">\r\n                                <label class=\"control-label\" for=\"itemPrice\">\r\n                                    Price\r\n                                <\/label>\r\n                                <input type=\"text\" class=\"itemPrice form-control\" id=\"itemPrice\" placeholder=\"0.00\" disabled>\r\n                                <input type=\"hidden\" name=\"dealersPrice\" \/>\r\n                                <input type=\"hidden\" name=\"suggestedRetailPrice\" \/>\r\n                            <\/div>\r\n                            <div class=\"form-group col-md-1 col-xs-6\">\r\n                                <label class=\"control-label\" for=\"itemQuantity\">\r\n                                    Quantity\r\n                                <\/label>\r\n                                <input type=\"number\" class=\"itemQuantity form-control\" id=\"itemQuantity\" placeholder=\"0\">\r\n                            <\/div>\r\n                            \r\n                            <div class=\"form-group col-md-2 col-xs-9\">\r\n                                <label class=\"control-label\" for=\"subtotal\">\r\n                                    Subtotal\r\n                                <\/label>\r\n                                <input type=\"text\" class=\"form-control\" id=\"subtotal\" placeholder=\"0.00\" disabled>\r\n                            <\/div>\r\n                            <div class=\"col-md-1 col-xs-3\">\r\n                                <a class=\"btn btn-danger remove-btn\" href=\"#\">\r\n                                    <i class=\"fa fa-trash-o fa-lg\"><\/i>\r\n                                <\/a>\r\n                            <\/div>\r\n                        <\/fieldset>\r\n                <\/div><!--\/.well-->\r\n            <\/div>"
    $("#checkout-items-list").append(addCheckoutRow);
});

$("body").on("click", ".remove-btn", function (e) {
		$(this).parentsUntil(".row").remove();
});

