package org.brightworks.genesis.inventory.service.impl;

import org.brightworks.genesis.inventory.dto.PictureDTO;
import org.brightworks.genesis.inventory.model.ProductPicture;
import org.brightworks.genesis.inventory.repo.ProductPictureRepo;
import org.brightworks.genesis.inventory.service.ProductPictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by KyelDavid on 1/17/2015.
 */
@Service
@Transactional
public class ProductPictureServiceImpl implements ProductPictureService {

    @Autowired
    private ProductPictureRepo pictureRepo;

    @Override
    public PictureDTO findById(Long id) {
        PictureDTO pictureDTO = new PictureDTO();
        ProductPicture pictureModel = pictureRepo.findOne(id);
        copyBytePicture(pictureDTO,pictureModel);
        return pictureDTO;
    }

    private void copyBytePicture(PictureDTO pictureDTO,ProductPicture pictureModel) {
        if(pictureModel != null && pictureModel.getPicture() != null) {
            byte[] bytePicture = new byte[pictureModel.getPicture().length];
            System.arraycopy(pictureModel.getPicture(), 0, bytePicture, 0, pictureModel.getPicture().length);
            pictureDTO.setPicture(bytePicture);
        }
    }
}
