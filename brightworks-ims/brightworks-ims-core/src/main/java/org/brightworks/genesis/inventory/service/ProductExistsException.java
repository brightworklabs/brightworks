package org.brightworks.genesis.inventory.service;

/**
 * Created by kyel on 4/2/16.
 */
public class ProductExistsException extends Exception {

    public ProductExistsException(String err) {
        super(err);
    }
}
