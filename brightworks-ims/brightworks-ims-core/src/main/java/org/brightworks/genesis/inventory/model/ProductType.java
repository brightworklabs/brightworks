package org.brightworks.genesis.inventory.model;

/**
 * Created by kyel on 12/18/2014.
 */
public enum ProductType {
    SERVICE,
    PRODUCT
}
