<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp" flush="true"/>
<body>
<jsp:include page="includes/page-header.jsp" flush="true" />

<!-- Content-->
<div class="container-fluid" id="container">

    <!-- Sidebar -->
    <jsp:include page="includes/side-bar.jsp" flush="true" />

    <!-- Page content -->
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            Reports
        </div><!--/.panel-heading-->

        <div class="panel-body" style="min-height: 200px;">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <h4 class="text-primary">Select Report</h4>
                    <select class="form-control">
                        <option>Sales Report</option>
                        <option>Profit Report</option>
                    </select>
                </div>
                
                <div class="col-md-6 col-sm-12">
                    <h4 class="text-primary">Filter Report</h4>
                    <div class="col-md-6 col-sm-12 no-padding-left"><input type="date" class="form-control" id="dateFrom"></div>
                    <div class="col-md-6 col-sm-12 no-padding-right"><input type="date" class="form-control" id="dateTo"></div>
                </div>
                
                <div class="col-md-3 col-sm-12">
                    <h4 class="text-primary">Generate Report</h4>
                    <button class="form-control btn btn-primary generate-btn" type="submit">Generate Report<i class="fa fa-file-o icon-right"></i></button>
                </div>
            </div><!--/.row-->

        </div><!--/.panel-body-->
    </div><!--/.panel-->

</div><!--/.container-->

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- Main JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
</body>

</html>
