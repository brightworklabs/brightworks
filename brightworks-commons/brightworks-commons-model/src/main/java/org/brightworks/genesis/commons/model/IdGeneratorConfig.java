package org.brightworks.genesis.commons.model;

/**
 * Created by kyeljohndavid on 3/26/2015.
 */
public final class IdGeneratorConfig {

    /**
     * Generator name.
     */
    public static final String GENERATOR_NAME = "DEFAULT_ID_GEN";


    /**
     * Table name of sequence.
     */
    public static final String TABLE_NAME = "SEQ_ENTITY_ID";

    /**
     * Id initial value.
     */
    public static final int ID_INITIAL_VALUE = 1000;

    private IdGeneratorConfig() {
    }
}
