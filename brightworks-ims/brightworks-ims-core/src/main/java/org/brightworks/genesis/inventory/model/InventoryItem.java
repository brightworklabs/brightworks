package org.brightworks.genesis.inventory.model;

import org.brightworks.genesis.commons.model.JpaModel;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kyel on 12/18/2014.
 */
@Deprecated
@Entity
@Table(name = "TXN_INVENTORY_ITEM")
public class InventoryItem extends JpaModel{

    @OneToOne
    @JoinColumn(name="CODE")
    private Product product;

    @Column(name = "STOCK")
    private int stock;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public BigDecimal dealersPrice(){
        return  product.getDp();
    }

    public BigDecimal cost(){
        return product.getCost();
    }

    public BigDecimal retailPrice(){
        return  product.getSrp();
    }
}
