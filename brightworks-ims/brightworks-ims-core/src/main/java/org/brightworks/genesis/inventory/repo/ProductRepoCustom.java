package org.brightworks.genesis.inventory.repo;

import org.brightworks.genesis.inventory.dto.ProductDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

/**
 * Created by kyeljohndavid on 3/31/2015.
 */
@Repository
public interface ProductRepoCustom {
    Page<ProductDTO> search(String term, Pageable pageable);
}
