<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!-- Sidebar -->
<div id="sidebar-container">
    <ul class="sidebar-nav">
        <li><a href="${pageContext.request.contextPath}/product/search"><i class="sidebar-nav-icon fa fa-search"></i>Product List</a></li>
        <sec:authorize access="hasRole('ROLE_CLERK')">
            <li><a href="${pageContext.request.contextPath}/receipt/search"><i class="sidebar-nav-icon fa fa-search"></i>Transaction Records</a></li>
        </sec:authorize>
        <li><a href="${pageContext.request.contextPath}/product/add"><i class="sidebar-nav-icon fa fa-plus"></i>Add Product</a></li>
        <sec:authorize access="hasRole('ROLE_CLERK')">
            <li><a href="${pageContext.request.contextPath}/product/checkout"><i class="sidebar-nav-icon fa fa-shopping-cart"></i>Checkout</a></li>
        </sec:authorize>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <li><a href="${pageContext.request.contextPath}/reports"><i class="sidebar-nav-icon fa fa-line-chart"></i>Generate Reports</a></li>
        </sec:authorize>
    </ul>
    <ul class="sidebar-nav bottom-sidebar-nav">
        <li><a href="${pageContext.request.contextPath}/j_spring_security_logout"><i class="sidebar-nav-icon fa fa-power-off"></i>Log Out</a></li>
    </ul>

</div>
