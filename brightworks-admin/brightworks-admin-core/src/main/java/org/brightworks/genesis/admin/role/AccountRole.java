package org.brightworks.genesis.admin.role;

/**
 * Created by kyeljohndavid on 4/15/2015.
 */
public enum AccountRole {
    ROLE_ADMIN("ROLE_ADMIN","Admin"),
    ROLE_CLERK("ROLE_CLERK","Clerk"),
    ROLE_CASHIER("ROLE_ACCOUNTING","Accounting");

    private  String role;

    private  String name;

    private AccountRole(String roleString,String name){
        role=roleString;
        this.name=name;
    }

    @Override
    public String toString(){
        return  role;
    }

    public String getCode(){
        return role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
