package org.brightworks.genesis.commons.security.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kyeljohndavid on 10/29/14.
 */
public class UserAuthRequest {

    private UserAuthDto userAuthDto = new UserAuthDto();

    private List<String> roles = new ArrayList<String>();

    public UserAuthRequest(UserAuthDto dto){
        this.userAuthDto = dto;
    }

    public UserAuthRequest(){}

    public UserAuthDto getUserAuthDto() {
        return userAuthDto;
    }

    public void setUserAuthDto(UserAuthDto userAuthDto) {
        this.userAuthDto = userAuthDto;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}

