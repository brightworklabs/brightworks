package org.brightworks.genesis.commons.security;

import org.brightworks.genesis.commons.security.dto.UserAuthDto;
import org.brightworks.genesis.commons.security.dto.UserAuthResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by kyel on 11/16/2014.
 */
@Service
public class TenantAccountAuthServiceImpl implements TenantAccountAuthService {
    private static String HOST = "localhost";

    private static String PORT = "8081";

    private static String API_URI = "/api/account/";

    private static String AUTH_ENDPOINT = "authenticate";

    private static String SCHEME = "http://";

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public UserAuthResponse authenticate(UserAuthDto authDto) throws AuthenticationException {

        UserAuthResponse authResponse = restTemplate.
                postForObject(SCHEME + HOST + ":" + PORT + API_URI + AUTH_ENDPOINT, authDto, UserAuthResponse.class);

        if(authResponse.isAuthenticated()){
            return authResponse;
        }else{
            throw new BadCredentialsException("Invalid Username or Password");
        }
    }
}
