package org.brightworks.genesis.inventory.repo;

import org.brightworks.genesis.inventory.model.ProductPicture;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by KyelDavid on 1/15/2015.
 */
public interface ProductPictureRepo extends JpaRepository<ProductPicture,Long> {
}
