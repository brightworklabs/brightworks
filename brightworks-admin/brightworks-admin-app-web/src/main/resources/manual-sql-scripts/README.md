SQL Scripts included here will be needed to run manually


Execute this query to get all  of the tenants1

SELECT string_agg(
    format('(SELECT * FROM %I.txn_line_item)', nspname),
           ' UNION ALL ')
FROM pg_namespace
WHERE nspname !~ '^(pg_.*|information_schema|public)$';

Use the result to build the view, it will be something like this

"(SELECT * FROM admin.txn_line_item) UNION ALL (SELECT * FROM mmnla.txn_line_item)"

WHERE IT JOINS ALL TENANT TXN_LINE_ITEM
