package org.brightworks.genesis.admin.dto;

/**
 * Created by kyeljohndavid on 4/17/2015.
 */
public class TenantInfoDTO {

    private Long id;

    private String tenantCode;

    private String tenantDescription;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getTenantDescription() {
        return tenantDescription;
    }

    public void setTenantDescription(String tenantDescription) {
        this.tenantDescription = tenantDescription;
    }

    public boolean isNew(){
        return (id == null);
    }
}
