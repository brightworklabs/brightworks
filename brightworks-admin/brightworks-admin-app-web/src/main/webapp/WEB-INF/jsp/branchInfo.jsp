<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp" flush="true"/>
<body>
<jsp:include page="includes/page-header.jsp" flush="true" />

<!-- Content-->
<div class="container-fluid" id="container">

    <!-- Sidebar -->
    <jsp:include page="includes/side-bar.jsp" flush="true" />

    <!-- Page content -->
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-8 no-padding">
                    Branch Information
                </div>

                <div class="col-xs-1 pull-right text-right no-padding">
                    <a href="#" data-toggle="dropdown" class="product-panel-dropdown"><i class="fa fa-wrench"></i></a>

                    <ul class="product-panel-dropdown-menu dropdown-menu" role="menu">
                        <li><a href="#"><i class="fa fa-pencil fa-sm icon-left"></i> Edit Branch Information</a></li>
                        <li><a href="#"><i class="fa fa-ban fa-sm icon-left"></i> Deactivate Branch Account</a></li>
                    </ul>

                </div>

            </div><!--/.row-->

        </div><!--/.panel-heading-->

        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-xs-12">

                        <h1 class="text-primary text-center">${tenant.tenantDescription}</h1>
                        <h4 class="text-center">${tenant.tenantCode}</h4>
                        <hr>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                        <h3 class="text-primary">Branch Users</h3>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-primary">
                                        <th>Username</th>
                                        <th>Full Name</th>
                                        <th>Role</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="user" items="${users}" varStatus="i">
                                    <tr>
                                        <td>${user.username}</td>
                                        <td>${user.firstName} ${user.lastName}</td>
                                        <td>${user.accountRole.name}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div><!--/.row-->

        </div><!--/.panel-body-->
    </div><!--/.panel-->

</div><!--/.container-->

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- Main JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
</body>

</html>
