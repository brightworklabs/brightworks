package org.brightworks.genesis.inventory.dto;

/**
 * Created by KyelDavid on 1/17/2015.
 */
public class PictureDTO {


    public PictureDTO(){

    }

    public  PictureDTO(byte[] picture){
        this.picture = picture;
    }
    private byte[] picture;

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public boolean hasPicture(){
        if(picture == null){
            return true;
        }else{
            return false;
        }
    }
}
