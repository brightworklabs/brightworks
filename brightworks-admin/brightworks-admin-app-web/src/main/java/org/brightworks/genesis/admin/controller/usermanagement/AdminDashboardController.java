package org.brightworks.genesis.admin.controller.usermanagement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by kyeljohndavid on 4/13/2015.
 */
@Controller
public class AdminDashboardController {

    private Logger logger = LoggerFactory.getLogger(AdminDashboardController.class);


    @RequestMapping(value="/dashboard")
    public String displayDashboard() {
         return "dashboard";
    }
}
