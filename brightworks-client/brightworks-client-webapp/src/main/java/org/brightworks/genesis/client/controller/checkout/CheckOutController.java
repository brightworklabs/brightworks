package org.brightworks.genesis.client.controller.checkout;

import org.brightworks.genesis.client.forms.CheckOutCounter;
import org.brightworks.genesis.client.forms.CheckOutForm;
import org.brightworks.genesis.client.util.MavBuilder;
import org.brightworks.genesis.inventory.dto.CheckOutItem;
import org.brightworks.genesis.inventory.dto.ProductDTO;
import org.brightworks.genesis.inventory.dto.TransactionRecordDTO;
import org.brightworks.genesis.inventory.service.CheckOutService;
import org.brightworks.genesis.inventory.service.ProductService;
import org.brightworks.genesis.inventory.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KyelDavid on 2/19/2015.
 */

@Controller
public class CheckOutController {

    private Logger logger = LoggerFactory.getLogger(CheckOutController.class);


    private String CHECK_OUT_PAGE = "checkOutForm";

    private  String RECEIPT = "receipt";
    /**
     * Checkout counter is Defined at client-mvc-ctx.xml
     * Acts as as a Check Out Item Container for the whole session
     *
     */
    @Autowired
    private CheckOutCounter checkOutCounter;

    @Autowired
    private ProductService productService;

    @Autowired
    private CheckOutService checkOutService;

    @Autowired
    private TransactionService transactionService;

    /**
     * We are persisting the checkout form for the purpose
     * of maintaining the contents of the form even if the user is navigating
     * the web page. Checkout form will be destroyed once the user
     * has logged out
     */
    @RequestMapping("/product/checkout")
    public ModelAndView checkoutForm(){
        List<CheckOutItem> checkOutItems = null;

        /**
         * Default number of entries for each checkout item
         */
        if(checkOutCounter.isEmpty()){
            checkOutItems = new ArrayList<>();
            checkOutItems.add(new CheckOutItem());
            checkOutItems.add(new CheckOutItem());
            checkOutCounter.add(checkOutItems);
        }else{
            checkOutItems = checkOutCounter.getItems();
        }

        CheckOutForm checkOutForm = new CheckOutForm(checkOutItems);

        return MavBuilder
                .render("checkout")
                .addAttr(CHECK_OUT_PAGE, checkOutForm)
                .toMav();
    }

    /**
     *  TODO: ADD checkout item cost on the form
     * @param checkOutForm
     * @param result
     * @return
     */
    @RequestMapping(value = "/product/checkout",method = RequestMethod.POST)
    public ModelAndView submitCheckOutForm(@ModelAttribute("checkOutForm") CheckOutForm checkOutForm,BindingResult result){
        List<CheckOutItem> checkOutItems = checkOutForm.getItems();
        TransactionRecordDTO txnRecordDto = checkOutService.checkOut(checkOutItems);
        checkOutCounter.clear();
        return MavBuilder
            .render("receipt")
            .addAttr(RECEIPT,txnRecordDto)
            .toMav();
    }

    @RequestMapping(value = "/ajax/product/checkout")
    public @ResponseBody
    ResponseEntity<String> ajaxAddToCart(@RequestParam(value="itemCode")String itemCode){

        ProductDTO product = productService.findByCode(itemCode);

        logger.debug("Adding Item To Cart: {}",product);

        if(product.isAvailable()) {
            checkOutCounter.add(new CheckOutItem(product));
            logger.debug("Successfully Added Item To Cart: {}",product);
            return new ResponseEntity<>("Item Successfully Added to Cart",HttpStatus.OK);
        }

        logger.debug("Failed to Add Item To Cart: {}",product);

        return  new ResponseEntity<>("Item is out of Stock",HttpStatus.NOT_ACCEPTABLE);

    }

    @RequestMapping(value = "/ajax/product/remove/{index}")
    public @ResponseBody ResponseEntity<String> ajaxRemoveItemToCart(@PathVariable(value="index")int index){
        checkOutCounter.remove(index);
        return  new ResponseEntity<>("Item has been removed",HttpStatus.NOT_ACCEPTABLE);

    }
}
