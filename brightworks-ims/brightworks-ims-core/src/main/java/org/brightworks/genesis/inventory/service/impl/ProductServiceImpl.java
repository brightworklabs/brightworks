package org.brightworks.genesis.inventory.service.impl;

import org.brightworks.genesis.commons.security.SecurityContextService;
import org.brightworks.genesis.inventory.dto.ProductDTO;
import org.brightworks.genesis.inventory.dto.TenantInfoResponse;
import org.brightworks.genesis.inventory.mapper.ProductMapper;
import org.brightworks.genesis.inventory.model.Price;
import org.brightworks.genesis.inventory.model.Product;
import org.brightworks.genesis.inventory.model.ProductPicture;
import org.brightworks.genesis.inventory.model.ProductType;
import org.brightworks.genesis.inventory.repo.ProductPictureRepo;
import org.brightworks.genesis.inventory.repo.ProductRepo;
import org.brightworks.genesis.inventory.service.ProductExistsException;
import org.brightworks.genesis.inventory.service.ProductService;
import org.brightworks.genesis.inventory.service.TenantInformationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kyel on 12/22/2014.
 */
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    private Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    private ProductRepo productRepo;

    @Autowired
    private ProductPictureRepo pictureRepo;

    @Autowired
    private TenantInformationService tenantInformationService;

    @Autowired
    private SecurityContextService securityContextService;

    public Product saveProduct(ProductDTO productDTO) throws ProductExistsException {
        logger.info("Saving Product: {}", productDTO);

        Product savedProduct = null;
        boolean isAdmin = securityContextService.currentTenantHasRole("ROLE_ADMIN");
        if(isAdmin){
            logger.info("Saving Product to all tenants: {}", productDTO);
            List<TenantInfoResponse> tenantInfoResponses = tenantInformationService.getAllTenants();
            for(TenantInfoResponse tenantInfoResponse: tenantInfoResponses){
                securityContextService.setAnonymousActiveTenant("ROLE_ADMIN",tenantInfoResponse.getTenantCode());
                savedProduct = performSave(productDTO);
            }
        }else{
            savedProduct = performSave(productDTO);
        }

        return  savedProduct;
    }

    private Product performSave(ProductDTO productDTO) throws ProductExistsException {
        Product product = toProductModel(productDTO);
        Product existingProduct = productRepo.findByCodeIgnoreCase(product.getCode());
        if(existingProduct != null && product.getId() == null) {
            logger.info("Existing Product exists. Skipping");
            throw new ProductExistsException("Product with item code of " + product.getCode()+"already exists");
        }else {
            if(productDTO.getPicture() != null && productDTO.getPicture().length !=0){
                logger.debug("Saving Product Image: {}",productDTO.getPicture());
                ProductPicture inventoryItemPicture = pictureRepo.save(new ProductPicture(productDTO.getPicture()));
                product.setPicture(inventoryItemPicture);
            }else{
                //Create Empty picture
                ProductPicture picture= pictureRepo.save(new ProductPicture(productDTO.getPicture()));
                product.setPicture(picture);
            }
            product = productRepo.save(product);
        }
        return existingProduct != null ? existingProduct : product;
    }

    @Override
    public ProductDTO findByCode(String code) {
        logger.info("Searching For Product With Code: {}",code);

        Product productModel = productRepo.findByCode(code);
        return ProductMapper.INSTANCE.toDto(productModel);
    }

    @Override
    public ProductDTO findById(Long id) {
        logger.info("Searching for product with the ID : {}", id);

        Product productModel = productRepo.findOne(id);
        ProductDTO productDTO = ProductMapper.INSTANCE.toDto(productModel);

        logger.info("Successfully Retrieved Product with the ID : {}", id);

        return productDTO;
    }

    @Override
    public List<Product> findByManufacturer(String manufacturer) {
        return productRepo.findByManufacturer(manufacturer);
    }

    @Override
    public List<ProductDTO> findProductsByCode(String code){
        return toProductDTOs(productRepo.findProductsByCode(code));
    }

    @Override
    public List<ProductDTO> findProductsByName(String name){
        return toProductDTOs(productRepo.findProductsByName(name));
    }

    public List<ProductDTO> searchsProductByName(String name,Pageable pageable){
        logger.info("Searching for product with the name: {} for Page = {} and with Size = {}", name
            ,pageable.getPageNumber()
            ,pageable.getOffset());

        List<Product> products = productRepo.findByNameLike("%"+name+"%",pageable);

        return toProductDTOs(products);
    }

    @Override
    public Page<ProductDTO> search(String term, Pageable pageable) {
        return productRepo.search(term,pageable);
    }

    private List<ProductDTO> toProductDTOs(List<Product> products){
        List<ProductDTO> productDTOs = new ArrayList<ProductDTO>();
        for(Product pro : products){
            productDTOs.add(ProductMapper.INSTANCE.toDto(pro));
        }
        return  productDTOs;
    }

    private Product toProductModel(ProductDTO productDTO){
        logger.debug("Converting Product DTO to  Product Model {} ",productDTO);
        Product product = new Product();
        Price price = new Price();

        product.setId(productDTO.getId());
        product.setCode(productDTO.getCode());
        product.setManufacturer(productDTO.getManufacturer());
        product.setName(productDTO.getName());
        if(productDTO.getProducType() != null){
            product.setType(productDTO.getProducType().equalsIgnoreCase("PRODUCT")? ProductType.PRODUCT :ProductType.SERVICE);
        }
        product.setDescription(productDTO.getDescription());
        product.setLocation(productDTO.getLocation());
        price.setCost(productDTO.getCost());
        price.setDp(productDTO.getDp());
        price.setSrp(productDTO.getSrp());

        product.setPrice(price);
        product.setStock(productDTO.getStock());
        logger.debug("SuccessFully Transformed DTO to Model {} ",product);
        return  product;
    }
}
