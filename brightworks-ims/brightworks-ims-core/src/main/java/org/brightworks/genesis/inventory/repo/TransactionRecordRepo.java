package org.brightworks.genesis.inventory.repo;

import org.brightworks.genesis.inventory.model.transaction.TransactionRecord;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kyeljohndavid on 3/21/2015.
 */
public interface TransactionRecordRepo extends JpaRepository<TransactionRecord,Long>,TransactionRecordRepoCustom {

    TransactionRecord findByTransactionNumber(String transactionNumber);
}
