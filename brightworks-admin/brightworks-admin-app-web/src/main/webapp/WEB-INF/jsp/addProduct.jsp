<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<jsp:include page="includes/head.jsp" flush="true"/>
<body>
<!-- Header -->
<jsp:include page="includes/page-header.jsp" flush="true" />

<!-- Content-->
<div class="container-fluid" id="container">

    <!-- Sidebar -->
    <jsp:include page="includes/side-bar.jsp" flush="true" />

    <!-- Page content -->
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            Inventory Item Registration
        </div><!--/.panel-heading-->

        <div class="panel-body">
            <div class="row">
                <div class="col-md-9 col-lg-6 centered">
                    <form:form commandName="product" enctype="multipart/form-data" action="${pageContext.request.contextPath}/product/save">
                        <!-- Image Input -->
                        ${pageContext.request.contextPath}
                        <form:input path="id" type="hidden" class="form-control" />
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Product Image</label>
                            <div class="input-group image-preview">
                                <input type="text"  class="form-control image-preview-filename" disabled="disabled">

                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                    </button>

                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                        <span class="glyphicon glyphicon-folder-open"></span>
                                        <span class="image-preview-input-title">Browse</span>
                                        <form:input type="file" accept="image/png, image/jpeg, image/gif" path="productImage"/>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <!-- Product Number -->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Product Number</label>
                            <form:input path="code" type="text" class="form-control" />
                        </div>

                        <!-- Product Name-->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Product Name</label>
                            <form:input path="name" type="text" class="form-control"/>
                        </div>

                        <!-- Product Manufacturer -->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Manufacturer</label>
                            <form:input path="manufacturer" type="text" class="form-control"/>
                        </div>

                        <!-- Item Type and Stock -->
                        <div class="col-xs-8 form-group">
                            <label class="control-label">Inventory Item Type</label>
                            <form:select path="producType" class="form-control">
                                <option>Product</option>
                                <option>Service</option>
                            </form:select>
                        </div>
                        
                        <div class="col-xs-4 form-group required">
                            <label class="control-label">Available Stock</label>
                            <form:input path="stock" class="form-control" size="4" type="text"/>
                        </div>

                        <!-- Product Description -->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Product Description</label>
                            <form:textarea path="description" class="form-control" />
                        </div>

                        <!-- Price and Cost -->
                        <div class="col-xs-4 form-group required">
                            <label class="control-label">Suggested Retail Price</label>
                            <form:input path="srp" class="form-control" size="4" type="text"/>
                        </div>

                        <div class="col-xs-4 form-group required">
                            <label class="control-label">Dealers Price</label>
                            <form:input path="dp" class="form-control" type="text"/>
                        </div>
                        
                        <div class="col-xs-4 form-group required">
                            <label class="control-label">Item Cost</label>
                            <form:input path="cost" class="form-control" size="4" type="text"/>
                        </div>

                        <!--TODO:: WIll be autofilled by the search  automatically feature-->
                        <form:hidden path="cost" class="form-control"/>
                        <!-- Button -->
                        <div class="col-md-12 form-group">
                            <button class="form-control btn btn-primary submit-button" type="submit">Save Inventory Item <i class="fa fa-check-square-o"></i></button>
                        </div>
                    </form:form>
                </div>
            </div><!--/.row-->

        </div><!--/.panel-body-->

    </div><!--panel-->
</div><!--/.container-->

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- Login Page JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
</body>
</html>
