package org.brightworks.genesis.inventory.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.math.BigDecimal;

/**
 * Created by kyel on 12/18/2014.
 */
@Embeddable
public class Price{

    /**
     * Dealers Price - The amount of if the item is being
     * sold to a distributor or reseller
     * */
     @Column(name = "DEALERS_PRICE")
     private BigDecimal dp;

    /**
     * Suggested Retail Price - Amount of good if the item
     * is being sold to a consumer
     *
     * */
     @Column(name = "SUGGESTED_RETAIL_PRICE")
     private BigDecimal srp;

    /**
     * Price Acquired
     * */
    @Column(name= "COST")
    private BigDecimal cost;

    public BigDecimal getDp() {
        return dp;
    }

    public void setDp(BigDecimal dp) {
        this.dp = dp;
    }

    public BigDecimal getSrp() {
        return srp;
    }

    public void setSrp(BigDecimal srp) {
        this.srp = srp;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }
}
