package org.brightworks.genesis.admin.repo;

import org.brightworks.genesis.admin.dto.UserAccountDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

/**
 * Created by kyeljohndavid on 5/29/2015.
 */

@Repository
public interface UserAccountRepoCustom {

    Page<UserAccountDTO> search(String term, Pageable pageable);

}
