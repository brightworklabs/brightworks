package org.brightworks.genesis.admin.controller.ws;

import org.brightworks.genesis.admin.dto.TenantInfoDTO;
import org.brightworks.genesis.admin.service.TenantInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by kyeljohndavid on 5/17/2015.
 */
@RestController
public class TenantInfoResource {


    @Autowired
    private TenantInfoService tenantInfoService;


    @RequestMapping(value = "/api/branches", method = RequestMethod.GET, produces = "application/json")
    public List<TenantInfoDTO> getAll(){
        return tenantInfoService.getAll();
    }
}
