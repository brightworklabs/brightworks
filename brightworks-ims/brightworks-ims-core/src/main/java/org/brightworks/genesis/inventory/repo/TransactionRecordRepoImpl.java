package org.brightworks.genesis.inventory.repo;

import com.mysema.query.jpa.impl.JPAQuery;
import org.brightworks.genesis.inventory.model.transaction.QTransactionRecord;
import org.brightworks.genesis.inventory.model.transaction.TransactionRecord;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by kyeljohndavid on 6/21/2015.
 */
public class TransactionRecordRepoImpl implements TransactionRecordRepoCustom {

    @Autowired
    private EntityManager em;

    @Override
    public Page<TransactionRecord> filterByDateRange(DateTime from, DateTime to, Pageable page) {
        JPAQuery query = new JPAQuery(em);
        QTransactionRecord record = QTransactionRecord.transactionRecord;

        List<TransactionRecord> results;

        if(from == null && to == null){
            results = getAllTransactionRecords(query, record);
        }else{
            results = filterTransactionRecordsWithDateRange(from, to, query, record);
        }

        return new PageImpl<TransactionRecord>(results,page,query.count());
    }

    private List<TransactionRecord> filterTransactionRecordsWithDateRange(DateTime from, DateTime to, JPAQuery query, QTransactionRecord record) {
        List<TransactionRecord> results;
        results = query
            .from(record)
            .where(record.transactionDate.between(from, to))
            .orderBy(record.transactionDate.desc())
            .list(record);
        return results;
    }

    private List<TransactionRecord> getAllTransactionRecords(JPAQuery query, QTransactionRecord record) {
        List<TransactionRecord> results;
        results = query
            .from(record)
            .orderBy(record.transactionDate.desc()).list(record);
        return results;
    }


}
