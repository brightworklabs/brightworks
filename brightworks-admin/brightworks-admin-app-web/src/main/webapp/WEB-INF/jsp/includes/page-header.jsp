<%--
  Created by IntelliJ IDEA.
  User: kyeljohndavid
  Date: 11/27/14
  Time: 10:37 AM
  To change this template use File | Settings | File Templates.
--%>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/user/search">Genesis</a>
            <div class="sidebar-toggle" id="sidebar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </div>
        </div>
    </div>
</nav>
