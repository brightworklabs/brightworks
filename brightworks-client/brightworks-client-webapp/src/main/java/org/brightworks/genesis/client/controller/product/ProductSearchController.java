package org.brightworks.genesis.client.controller.product;

import org.brightworks.genesis.client.controller.datatables.DataTablesResponse;
import org.brightworks.genesis.inventory.dto.ProductDTO;
import org.brightworks.genesis.inventory.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by kyel on 3/3/2015.
 */
@RestController
public class ProductSearchController {

    @Autowired
    private ProductService productService;

    @RequestMapping("/product/ajax/search")
    public List<ProductDTO> findProductsByNameOrCode(@RequestParam(value="name",required = false) String name,
                                                     @RequestParam(value="code",required = false) String code) {
        if(name != null) {
            return productService.findProductsByName(name);
        }else{
            return productService.findProductsByCode(code);
        }
    }


    @RequestMapping(value = "/product/searchProduct",method = RequestMethod.GET)
    public DataTablesResponse<ProductDTO> dataTableSearchProduct(@RequestParam (required=true) int sEcho,
                                                                 @RequestParam (required=true) int iDisplayStart,
                                                                 @RequestParam (required=true) int iDisplayLength,
                                                                 @RequestParam (required=true) int iColumns,
                                                                 @RequestParam (required=true) int iSortCol_0,
                                                                 @RequestParam (required=false)String sSortDir_0,
                                                                 @RequestParam (required=true) String sSearch){


        int page = (int) Math.ceil(iDisplayStart/iDisplayLength);
        Page<ProductDTO> productPage = productService
                                .search(sSearch, new PageRequest(page, iDisplayLength));

        return new DataTablesResponse<>(productPage.getContent(),
            sEcho,
            productPage.getTotalElements(),
            productPage.getTotalElements());
    }
}
