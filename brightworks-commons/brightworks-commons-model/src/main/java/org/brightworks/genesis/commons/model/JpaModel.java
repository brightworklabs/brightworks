package org.brightworks.genesis.commons.model;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;

/**
 * To be extended of all entities to be persisted
 *
 * @author  by kyel on 10/5/2014.
 */
@MappedSuperclass
public abstract class JpaModel {


    /**
     * Generator name.
     */
    public static final String GENERATOR_NAME = "DEFAULT_ID_GEN";


    /**
     * Table name of sequence.
     */
    public static final String TABLE_NAME = "SEQ_ENTITY_ID";

    /**
     * Id initial value.
     */
    public static final int ID_INITIAL_VALUE = 1000;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = GENERATOR_NAME)
    @TableGenerator(name = GENERATOR_NAME, table = TABLE_NAME, initialValue = ID_INITIAL_VALUE)
    private Long id;

    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "created_date")
    private DateTime createdDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }
}
