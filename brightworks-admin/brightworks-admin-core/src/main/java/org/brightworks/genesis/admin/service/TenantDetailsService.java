package org.brightworks.genesis.admin.service;

import org.brightworks.genesis.admin.model.UserAccount;
import org.brightworks.genesis.admin.repo.UserAccountRepo;
import org.brightworks.genesis.commons.security.TenantUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kyel on 10/22/2014.
 */
@Service("tenantDetailsService")
public class TenantDetailsService implements UserDetailsService {

    @Autowired
    private UserAccountRepo userAccountRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserAccount account = userAccountRepo.findByUsername(username);
        if(account != null){
            List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            authorities.add(new SimpleGrantedAuthority(account.getRole().getRole()));
            return new TenantUserDetails(account.getUsername(),
                    account.getPassword(),authorities,account.getTenantCode());
        }else{
            throw new UsernameNotFoundException("Bad Credentials");
        }
    }
}
