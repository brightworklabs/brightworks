package org.brightworks.genesis.commons.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by kyel on 10/26/2014.
 */
public abstract class BaseAccountUserDetails extends User {

    public BaseAccountUserDetails(){
        super(" "," ",new ArrayList<GrantedAuthority>());
    }

    public BaseAccountUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public BaseAccountUserDetails(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }
}
