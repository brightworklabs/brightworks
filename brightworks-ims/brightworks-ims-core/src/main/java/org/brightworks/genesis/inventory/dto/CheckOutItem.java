package org.brightworks.genesis.inventory.dto;

import org.brightworks.genesis.inventory.dto.ProductDTO;

/**
 * Line Item for every item that's in checkout
 */
public class CheckOutItem extends ProductDTO{

    /**
     * Responsible for identifying if the
     * product is being sold as a dealers price
     * or as suggested retail price
     */
    private String priceIdentifier;

    /**
     * Quantity of items to be bought
     */
    private int quantity;

    public CheckOutItem(){}

    public CheckOutItem(ProductDTO productDTO){
        this.setCode(productDTO.getCode());
        this.setDescription(productDTO.getDescription());
        this.setId(productDTO.getId());
        this.setManufacturer(productDTO.getManufacturer());
        this.setName(productDTO.getName());
        this.setPicture(productDTO.getPicture());
        this.setPictureId(productDTO.getPictureId());
        this.setProducType(productDTO.getProducType());
        this.setSrp(productDTO.getSrp());
        this.setDp(productDTO.getDp());
        this.setCost(productDTO.getCost());
        this.setLocation(productDTO.getLocation());
        this.quantity = 1;
    }

    public String getPriceIdentifier() {
        return priceIdentifier;
    }

    public void setPriceIdentifier(String priceIdentifier) {
        this.priceIdentifier = priceIdentifier;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
