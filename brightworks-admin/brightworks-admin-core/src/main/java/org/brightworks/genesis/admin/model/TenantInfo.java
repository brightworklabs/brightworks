package org.brightworks.genesis.admin.model;

import org.brightworks.genesis.commons.model.JpaModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kyeljohndavid on 4/15/2015.
 */

@Table(name="TENANT_INFO")
@Entity
public class TenantInfo extends JpaModel {

    @Column(name = "TENANT_CODE",unique = true)
    private String tenantCode;

    @Column(name = "TENANT_DESCRIPTION")
    private String tenantDescription;

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getTenantDescription() {
        return tenantDescription;
    }

    public void setTenantDescription(String tenantDescription) {
        this.tenantDescription = tenantDescription;
    }
}
