package org.brightworks.genesis.admin.controller.usermanagement;

import org.brightworks.genesis.admin.controller.validator.AddUserValidator;
import org.brightworks.genesis.admin.dto.UserAccountDTO;
import org.brightworks.genesis.admin.forms.UserAccountForm;
import org.brightworks.genesis.admin.role.AccountRole;
import org.brightworks.genesis.admin.service.TenantInfoService;
import org.brightworks.genesis.admin.service.UserAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Arrays;

import static org.brightworks.genesis.admin.util.MavBuilder.render;

@Controller
@RequestMapping("/user")
public class AddUserController {

    private Logger logger = LoggerFactory.getLogger(AddUserController.class);

    private static String ADD_USER_VIEW = "addUser";


    private static String EDIT_USER_VIEW = "editUser";


    private static String USER_VIEW = "userInfo";

    @Autowired
    private TenantInfoService tenantInfoService;

    @Autowired
    private UserAccountService userAccountService;

    @Autowired
    private AddUserValidator userValidator;

    @RequestMapping(value = "/add")
    public ModelAndView addUserAccountForm() {
        return render(ADD_USER_VIEW)
            .addAttr("ACCOUNT_ROLES", Arrays.asList(AccountRole.values()))
            .addAttr("TENANTS", tenantInfoService.getAll())
            .addAttr("account", new UserAccountForm())
            .toMav();
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView saveUserAccount(@ModelAttribute("account") UserAccountForm userAccount, BindingResult result) {
        userValidator.validate(userAccount, result);
        if (result.hasErrors()) {
            return render(ADD_USER_VIEW)
                .addAttr("ACCOUNT_ROLES", Arrays.asList(AccountRole.values()))
                .addAttr("TENANTS", tenantInfoService.getAll())
                .addAttr("result", result)
                .addAttr("account", userAccount)
                .toMav();
        } else {
            UserAccountDTO dto = userAccountService.saveAccountInformation(userAccount.getUserAccount());
            return new ModelAndView(new RedirectView("/user/view/"+dto.getId()));
        }
    }

    @RequestMapping(value="/view/{id}")
    public ModelAndView view(@PathVariable("id")Long userId) {
        return render(USER_VIEW)
            .addAttr("account", userAccountService.findById(userId))
            .toMav();

    }

    @RequestMapping(value="/edit/{id}")
    public ModelAndView edit(@PathVariable("id")Long userId) {
        UserAccountForm userAccountForm = new UserAccountForm();
        userAccountForm.setUserAccount(userAccountService.findById(userId));
        return render(EDIT_USER_VIEW)
                .addAttr("account", userAccountForm)
                .addAttr("ACCOUNT_ROLES", Arrays.asList(AccountRole.values()))
                .addAttr("TENANTS", tenantInfoService.getAll())
                .toMav();

    }

}
