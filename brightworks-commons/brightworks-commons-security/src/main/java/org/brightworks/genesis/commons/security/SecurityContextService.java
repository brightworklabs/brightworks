package org.brightworks.genesis.commons.security;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kyeljohndavid on 4/26/2015.
 */
@Service
public class SecurityContextService {

    public String getCurrentTenant(){
        TenantUserDetails currentTenant = (TenantUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return currentTenant.getTenantCode();
    }

    public TenantUserDetails getCurrentTenantUserDetails(){
        return (TenantUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public boolean currentTenantHasRole(final String role){
        return getCurrentTenantUserDetails().getRoles().contains(role);
    }

    public void setAnonymousActiveTenant(String role, String tenantCode){
        Authentication authCtx = SecurityContextHolder.getContext().getAuthentication();
        if (authCtx != null && !authCtx.getClass().isAssignableFrom(AnonymousAuthenticationToken.class)) {
            // Log a warning for a possible bug related scenario. This should not occur.
        }
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        if(role.equals("ROLE_ADMIN")){
            authorities.add(new SimpleGrantedAuthority(role));
            TenantUserDetails tenantUserDetails = new TenantUserDetails("anonymous",
                "anonymous",
                authorities,
                tenantCode);
            SecurityContextHolder.getContext().setAuthentication(new TenantAuthenticationToken(tenantUserDetails));
        }
    }
}
