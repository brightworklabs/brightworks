package org.brightworks.genesis.client.controller.reports;

/**
 * Created by kyeljohndavid on 5/29/2015.
 */
public class ReportNames {

    public static final String PROFIT_REPORT = "rprt_profit_report_v3";


    public static final String SALES_REPORT = "rprt_sales";

}
