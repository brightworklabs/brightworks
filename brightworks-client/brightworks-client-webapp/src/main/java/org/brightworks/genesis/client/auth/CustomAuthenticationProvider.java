package org.brightworks.genesis.client.auth;

import org.brightworks.genesis.commons.security.TenantAccountAuthService;
import org.brightworks.genesis.commons.security.TenantAuthenticationToken;
import org.brightworks.genesis.commons.security.TenantUserDetails;
import org.brightworks.genesis.commons.security.dto.UserAuthDto;
import org.brightworks.genesis.commons.security.dto.UserAuthResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by kyel on 11/20/2014.
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider{

    private Logger logger = LoggerFactory.getLogger(CustomAuthenticationProvider.class);

    @Autowired
    private TenantAccountAuthService tenantAccountAuthService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String username = authentication.getName();
        String password = (String) authentication.getCredentials();

        UserAuthDto authDto = new UserAuthDto(username,password);
        logger.debug("Performing Authentication for user with username: "+username);
        try{
            UserAuthResponse authResponse = tenantAccountAuthService.authenticate(authDto);
            UserAuthDto userAuth = authResponse.getUserAuthDto();

            TenantUserDetails tenantUserDetails = new TenantUserDetails(username,
                    password,
                    toGrantedAuthorities(userAuth.getRoles()),
                    userAuth.getTenantCode());

            return new TenantAuthenticationToken(tenantUserDetails);
        }catch(BadCredentialsException ae){
            throw new BadCredentialsException("Bad Credentials");
        }
    }

    private Collection<? extends  GrantedAuthority> toGrantedAuthorities(List<String> strAuth){
        List<GrantedAuthority>authorities = new ArrayList<GrantedAuthority>();
        for(String str: strAuth){
            authorities.add(new SimpleGrantedAuthority(str));
        }
        return  authorities;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}
