package org.brightworks.genesis.commons.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by kyel on 11/2/2014.
 */
public class TenantAuthenticationToken extends AbstractAuthenticationToken {

    private final Object principal;

    private Object credentials;

    /**
     *
     * @param principal the TenantUserDetails Object
     * @param credentials The password of the Tenant User Detail
     * @param authorities Role of the tenant
     */
    public TenantAuthenticationToken(Object principal,Object credentials,Collection<? extends GrantedAuthority> authorities){
        super(authorities);
        this.principal = principal;
        this.credentials = credentials;
        super.setAuthenticated(true);
    }

    /**
     *
     * @param tenantUserDetails the TenantUserDetails Object
     *  From the tenant user details, we will extract the information that is contained there
     *  The TenantUserDetails is just a container for credentials, but also be used for other purposes
     */
    public TenantAuthenticationToken(TenantUserDetails tenantUserDetails){
        super(tenantUserDetails.getAuthorities());
        this.principal = tenantUserDetails;
        this.credentials = tenantUserDetails.getPassword();
        super.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return credentials;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
        credentials = null;
    }
}
