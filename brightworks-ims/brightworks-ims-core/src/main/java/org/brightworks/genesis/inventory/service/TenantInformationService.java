package org.brightworks.genesis.inventory.service;

import org.brightworks.genesis.inventory.dto.TenantInfoResponse;

import java.util.List;

/**
 * Created by kyeljohndavid on 5/17/2015.
 */
public interface TenantInformationService {

    List<TenantInfoResponse> getAllTenants();
}
