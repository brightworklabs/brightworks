package org.brightworks.genesis.inventory.model.transaction;

import org.brightworks.genesis.commons.model.JpaModel;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Will also Serve as Official Receipt
 * Created by kyeljohndavid on 3/21/2015.
 */
@Entity
@Table(name = "TXN_TRANSACTION_RECORD")
public class TransactionRecord extends JpaModel{

    /**
     * Unique Identifier for all transactions
     */
    @Column(name = "TRANSACTION_NUMBER", unique = true)
    private String transactionNumber;

    /**
     * Items that has been sold.
     */
    @OneToMany(mappedBy = "transactionRecord",fetch = FetchType.EAGER)
    private List<LineItem> lineItems;

    /**
     * Date when the transaction  has occurred
     */
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "transaction_date")
    private DateTime transactionDate;

    /**
     *  Check if the transaction
     *  record has been cancelled
     **/
    @Column(name  ="VALID")
    private boolean valid;

    /**
     * Sum of the items purchased
     */
    @Column
    private BigDecimal total;

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public DateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(DateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
