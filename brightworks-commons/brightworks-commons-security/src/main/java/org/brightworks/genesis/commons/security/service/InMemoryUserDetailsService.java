package org.brightworks.genesis.commons.security.service;

import org.brightworks.genesis.commons.security.TenantUserDetails;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kyel on 10/22/2014.
 */
public class InMemoryUserDetailsService implements UserDetailsService {
    private final Map<String,String> accountMap = new HashMap<String,String>();
    private final Map<String,List<GrantedAuthority>> accountRoles  = new HashMap<String, List<GrantedAuthority>>();
    private final Map<String,String> accountTenant = new HashMap<String, String>();
    {
        accountMap.put("admin","F6XBa0W01K9zj2nRQQP3x8gbGpKzSatGtSdapemDpcNxkBL7xkoc8OVOhHrvVxZB");
        accountMap.put("clerk","F6XBa0W01K9zj2nRQQP3x8gbGpKzSatGtSdapemDpcNxkBL7xkoc8OVOhHrvVxZB");
        accountMap.put("cashier","F6XBa0W01K9zj2nRQQP3x8gbGpKzSatGtSdapemDpcNxkBL7xkoc8OVOhHrvVxZB");

        List<GrantedAuthority> admin = new ArrayList<GrantedAuthority>();
        admin.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

        List<GrantedAuthority> clerk = new ArrayList<GrantedAuthority>();
        clerk.add(new SimpleGrantedAuthority("ROLE_CLERK"));


        List<GrantedAuthority> cashier= new ArrayList<GrantedAuthority>();
        cashier.add(new SimpleGrantedAuthority("ROLE_CASHIER"));

        accountRoles.put("admin",admin);
        accountRoles.put("clerk",clerk);
        accountRoles.put("cashier",cashier);

        accountTenant.put("admin","ADMIN");
        accountTenant.put("clerk","TENANT1");
        accountTenant.put("cashier","TENANT2");
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return  new TenantUserDetails(username,
                accountMap.get(username) == null ? "" :accountMap.get(username),
                accountRoles.get(username) == null ?  new ArrayList<GrantedAuthority>():accountRoles.get(username)
                ,accountTenant.get(username) == null ? "" :accountTenant.get(username));
    }
}
