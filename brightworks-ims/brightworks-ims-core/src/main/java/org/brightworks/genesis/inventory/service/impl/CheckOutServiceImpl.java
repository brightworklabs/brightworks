package org.brightworks.genesis.inventory.service.impl;

import org.brightworks.genesis.inventory.dto.CheckOutItem;
import org.brightworks.genesis.inventory.dto.LineItemDTO;
import org.brightworks.genesis.inventory.dto.TransactionRecordDTO;
import org.brightworks.genesis.inventory.mapper.LineItemMapper;
import org.brightworks.genesis.inventory.mapper.TransactionRecordMapper;
import org.brightworks.genesis.inventory.model.Product;
import org.brightworks.genesis.inventory.model.transaction.LineItem;
import org.brightworks.genesis.inventory.model.transaction.TransactionRecord;
import org.brightworks.genesis.inventory.repo.LineItemRepo;
import org.brightworks.genesis.inventory.repo.ProductRepo;
import org.brightworks.genesis.inventory.repo.TransactionRecordRepo;
import org.brightworks.genesis.inventory.service.CheckOutService;
import org.brightworks.genesis.inventory.service.ProductService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by kyeljohndavid on 3/25/2015.
 */
@Service
@Transactional
public class CheckOutServiceImpl implements CheckOutService{

    @Autowired
    private ProductService productService;

    @Autowired
    private LineItemRepo lineItemRepo;

    @Autowired
    private TransactionRecordRepo transactionRecordRepo;

    @Autowired
    private ProductRepo productRepo;
    //TODO: Add DTO

    @Override
    public TransactionRecordDTO checkOut(List<CheckOutItem> checkOutItems) {
        TransactionRecord transactionRecord = new TransactionRecord();
        DateTime currDateTime = DateTime.now();
        DateTimeFormatter dtf = DateTimeFormat.forPattern("MMddyyyy");
        BigDecimal total = BigDecimal.ZERO;
        DateTime transactionDate = DateTime.now();
        transactionRecord.setTransactionDate(transactionDate);
        transactionRecord.setValid(true);
        String uuid = UUID.randomUUID().toString().toUpperCase().replace("-","");
        //Current Date + Random String from UUID
        transactionRecord.setTransactionNumber(( dtf.print(currDateTime) + uuid.substring(uuid.length() - 6)));
        List<LineItem> items = processItems(checkOutItems,transactionDate);

        for(LineItem item: items){
            item.setTransactionRecord(transactionRecord);
        }

        //Compute grand total
        for(LineItem item: items){
            total = total.add(item.getTotal());
        }

        transactionRecord.setTotal(total);
        transactionRecord.setLineItems(items);
        TransactionRecord txnRecord = transactionRecordRepo.save(transactionRecord);
        List<LineItem> savedItems = lineItemRepo.save(items);
        updateInventory(savedItems);

        TransactionRecordDTO dto = TransactionRecordMapper.INSTANCE.toTransactionRecord(txnRecord);
        List<LineItemDTO> itemDTOs = LineItemMapper.INSTANCE.toLineItemDtos(savedItems);
        dto.setLineItems(itemDTOs);
        return  dto;
    }

    /**
     * Update the stocks of the current inventory
     * Subtracts the quantity ordered of the given line item
     * and its matching product onthe products table.
     * @param lineItems
     */
    private void updateInventory(List<LineItem> lineItems){
        List<Product> products = new ArrayList<Product>();
        for(LineItem item : lineItems){
            Product product = productRepo.findByCode(item.getItemCode());
            int ordererdQty = item.getQty();
            int newQty = product.getStock() - ordererdQty;
            product.setStock(newQty);
            products.add(product);
        }
        productRepo.save(products);
    }

    private List<LineItem> processItems(List<CheckOutItem> checkOutItems,DateTime transactionDate){
        List<LineItem> lineItems = new ArrayList<LineItem>();
        for(CheckOutItem checkOutItem: checkOutItems){
            lineItems.add(mapCheckOutItemToLineItem(checkOutItem, transactionDate));
        }
        return  lineItems;
    }

    private LineItem mapCheckOutItemToLineItem(CheckOutItem checkOutItem,DateTime transactionDate){
        return populateLineItem(checkOutItem, transactionDate);
    }

    private LineItem populateLineItem(CheckOutItem checkOutItem, DateTime transactionDate) {
        LineItem lineItem = new LineItem();
        lineItem.setItemCode(checkOutItem.getCode());
        lineItem.setItemName(checkOutItem.getName());
        lineItem.setCost(checkOutItem.getCost());
        lineItem.setSrp(checkOutItem.getSrp());
        lineItem.setDp(checkOutItem.getDp());
        lineItem.setQty(checkOutItem.getQuantity());
        lineItem.setTransactionDate(transactionDate);
        lineItem.setPriceType(identifyPriceType(checkOutItem.getPriceIdentifier()));
        lineItem.setTotal(computeTotal(checkOutItem));
        return lineItem;
    }

    private BigDecimal computeTotal(CheckOutItem checkOutItem) {
        if(checkOutItem.getPriceIdentifier().equalsIgnoreCase("srp")){
            return  checkOutItem.getSrp().multiply(new BigDecimal(checkOutItem.getQuantity()));
        }else{
            return  checkOutItem.getDp().multiply(new BigDecimal(checkOutItem.getQuantity()));
        }
    }

    private String identifyPriceType(String priceIdentifier) {
        return priceIdentifier.equalsIgnoreCase("srp") ? "srp" : "dp";
    }
}
