package org.brightworks.genesis.client.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Kurt on 11/20/2014.
 */
@Controller
public class ClientDashboardController  {

    private Logger logger = LoggerFactory.getLogger(ClientDashboardController.class);

    @RequestMapping(value="/dashboard")
    public String displayDashboard() {

        Object principal =
                SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return "dashboard";
    }
}
