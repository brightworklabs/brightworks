package org.brightworks.genesis.admin.mapper;

import org.brightworks.genesis.admin.dto.UserAccountDTO;
import org.brightworks.genesis.admin.model.UserAccount;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * Created by kyeljohndavid on 4/26/2015.
 */
@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface UserAccountMapper {

    UserAccountMapper INSTANCE =  Mappers.getMapper(UserAccountMapper.class);

    @Mappings(value = {
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "username", target = "username"),
        @Mapping(source = "name.givenName",target = "firstName"),
        @Mapping(source = "name.lastName",target = "lastName"),
        @Mapping(source = "role",target = "accountRole"),
        @Mapping(source = "tenantInfo.tenantCode",target = "tenantCode"),
        @Mapping(source = "tenantInfo.tenantDescription",target = "tenantDescription")
    })
    UserAccountDTO toDto(UserAccount account);

    List<UserAccountDTO> toUserAcountDtos(List<UserAccount> userAccounts);
}
