package org.brightworks.genesis.client.config.hibernate;

import org.brightworks.genesis.commons.security.TenantUserDetails;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by kyel on 12/5/2014.
 */

public class SchemaResolver implements CurrentTenantIdentifierResolver {

    /**
     * If the current principal is not a TenantUserDetails Object
     * classify him as anonymous user and only give him access to the
     * public database.
     */
    @Override
    public String resolveCurrentTenantIdentifier() {
        if(SecurityContextHolder.getContext().getAuthentication() != null
                && SecurityContextHolder.getContext().getAuthentication().getPrincipal() != null){
            Object userDetails = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if(userDetails instanceof TenantUserDetails){
                TenantUserDetails details =(TenantUserDetails)
                        SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                return details.getTenantCode();
            }else{
                return "public";
            }
        }
        return "public";
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return false;
    }
}