package org.brightworks.genesis.client.controller.reports;

import org.brightworks.genesis.client.app.Constants;
import org.brightworks.genesis.inventory.service.TenantInformationService;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.sql.DataSource;
import java.sql.Timestamp;

import static org.brightworks.genesis.client.util.RenderMavBuilder.render;

@Controller
public class ReportController {

    private Logger logger = LoggerFactory.getLogger(ReportController.class);

    private static final String REPORTS_VIEW = "reports";

    @Autowired
    @Qualifier("clientDataSource")
    private DataSource dataSource;

    @Autowired
    private TenantInformationService tenantInformationService;


    @RequestMapping(value="/reports")
    public ModelAndView displayReports() {

        logger.info("Displaying Reports Page");

        return render(REPORTS_VIEW)
            .addAttr("tenants",tenantInformationService.getAllTenants())
            .toMav();
    }
    //TODO: Refactor This
    @RequestMapping(value="/reports/profit",method = RequestMethod.GET)
    public ModelAndView generateProfitReport(@RequestParam("dateTo") String dateTo,
                                             @RequestParam("dateFrom") String dateFrom,
                                             @RequestParam("tenant") String tenant){

        logger.info("Generating Profit Reports");

        if(tenant.equals("ALL")){
            tenant = Constants.ADMIN_DB;
        }

        return render(ReportNames.PROFIT_REPORT)
            .addAttr("format","pdf")
            .addAttr("tenant", tenant)
            .addAttr("datasource",dataSource)
            .addAttr("dateTo", new Timestamp(new DateTime(dateTo).getMillis()))
            .addAttr("dateFrom",new Timestamp(new DateTime(dateFrom).getMillis()))
            .toMav();

    }

    @RequestMapping(value="/reports/sales",method = RequestMethod.GET)
    public ModelAndView generateSalesReport(@RequestParam("dateTo") String dateTo,
                                             @RequestParam("dateFrom") String dateFrom,
                                             @RequestParam("tenant") String tenant){

        logger.info("Generating Profit Reports");

        if(tenant.equals("ALL")){
            tenant = Constants.ADMIN_DB;
        }

        return render(ReportNames.SALES_REPORT)
            .addAttr("format","pdf")
            .addAttr("tenant", tenant)
            .addAttr("datasource",dataSource)
            .addAttr("dateTo", new Timestamp(new DateTime(dateTo).getMillis()))
            .addAttr("dateFrom",new Timestamp(new DateTime(dateFrom).getMillis()))
            .toMav();

    }
}
