package org.brightworks.genesis.commons.security;

import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by kyel on 10/22/2014.
 */
public class TenantUserDetails extends BaseAccountUserDetails {

    private String tenantCode;

    public  TenantUserDetails(){
        super();
    }

    public TenantUserDetails(String username, String password,
                             Collection<? extends GrantedAuthority> authorities,
                       String tenantCode) {
        super(username, password, authorities);
        this.tenantCode = tenantCode;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public List<String> getRoles(){
        List<String> roles = new ArrayList<String>();

        Collection<GrantedAuthority> grantedAuthorities =
                this.getAuthorities();
        for(GrantedAuthority ga : grantedAuthorities){
            roles.add(ga.getAuthority());
        }
        return  roles;
    }
}
