package org.brightworks.genesis.admin.controller;

import org.brightworks.genesis.admin.dto.UserAccountDTO;

import java.util.List;

/**
 * Created by kyeljohndavid on 5/29/2015.
 */
public class DataTablesResponse<T> {


    private List<UserAccountDTO> data;

    private int draw;

    private long recordsTotal;

    private long recordsFiltered;


    public DataTablesResponse(List<UserAccountDTO> data, int draw, long recordsTotal, long recordsFiltered) {
        this.data = data;
        this.draw = draw;
        this.recordsTotal = recordsTotal;
        this.recordsFiltered = recordsFiltered;
    }

    public DataTablesResponse(){}


    public List<UserAccountDTO> getData() {
        return data;
    }

    public void setData(List<UserAccountDTO> data) {
        this.data = data;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public long getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public long getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(long recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public void setRecordsFiltered(int recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }
}
