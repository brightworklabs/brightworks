package org.brightworks.genesis.admin.controller.usermanagement;

import org.brightworks.genesis.admin.forms.TenantForm;
import org.brightworks.genesis.admin.repo.TenantInfoRepo;
import org.brightworks.genesis.admin.service.TenantInfoService;
import org.brightworks.genesis.admin.service.UserAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import static org.brightworks.genesis.admin.util.MavBuilder.render;

@Controller
@RequestMapping("/branch")
public class AddBranchController  {

    private Logger logger = LoggerFactory.getLogger(AddBranchController.class);

    private static String ADD_BRANCH_VIEW = "addBranch";

    private static String BRANCH_OBJ = "tenant";

    private static String BRANCH_INFO_VIEW = "branchInfo";

    private final String ALL_TENANTS_PAGE = "tenantList";

    @Autowired
    private TenantInfoRepo tenantInfoRepo;

    @Autowired
    private TenantInfoService tenantInfoService;

    @Autowired
    private UserAccountService userAccountService;

    @RequestMapping("/view/{code}")
    public ModelAndView displayBranch(@PathVariable String code){
        return render(BRANCH_INFO_VIEW)
            .addAttr(BRANCH_OBJ, tenantInfoService.getTenantInfo(code))
            .addAttr("users", userAccountService.findByBranchCode(code))
            .toMav();
    }

    @RequestMapping(value="/add")
    public ModelAndView displayAddBranch() {
        return render(ADD_BRANCH_VIEW)
            .addAttr(BRANCH_OBJ, new TenantForm())
            .toMav();
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public ModelAndView saveTenant(@ModelAttribute("tenant") TenantForm tenantForm,BindingResult result){
        if(!tenantInfoService.exists(tenantForm.getCode())){
            tenantInfoService.save(tenantForm.getTenant());
            return render(ADD_BRANCH_VIEW)
                .addAttr("SUCCESS", "Successfully created branch " + tenantForm.getCode())
                .toMav();
        }else{
            return render(ADD_BRANCH_VIEW)
                .addAttr("ERROR", "Tenant " + tenantForm.getCode()+" Is Already Exists")
                .toMav();
        }
    }

    @RequestMapping("/all")
    public ModelAndView viewAllTenants() {
        return render(ALL_TENANTS_PAGE)
                .addAttr("tenants",tenantInfoRepo.findAll())
                .toMav();

    }
}
