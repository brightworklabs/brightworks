package org.brightworks.genesis.inventory.model.transaction;

import org.brightworks.genesis.commons.model.JpaModel;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author kyeljmd
 * When order items/inventory items has been finalized
 * their neceessary Data will be transfered here such as
 * item code, cost, srp, dealers price, and additonal flag
 * named as valid if it's been refunded or not.
 * this Domain model will act as a purchase/ transanction history
 * and how the product is sold(Dealers Price/SRP)
 * This will also be responsbile for generation of the reports
 */

@Entity
@Table(name = "TXN_LINE_ITEM")
public class LineItem extends JpaModel {

    /**
     * flag if the line item transaction has been refunded
     */
    @Column
    private boolean refunded;

    @Column
    private String itemCode;

    @Column
    private String itemName;

    @Column
    private BigDecimal srp;

    @Column
    private BigDecimal dp;

    @Column
    private BigDecimal cost;

    /**
     * flag if the line item transaction has been cancelled
     */
    @Column
    private boolean cancelled;

    @Column
    private int qty;

    /**
     * Total amount of this item ( qty * srp or dp depending
     * on how it was sold
     */
    @Column
    private BigDecimal total;

    /**
     * Identify the type of pricing this item has been sold
     */
    @Column
    private String priceType;

    /**
     *  Transaction Record where this line item was included
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "transactionRecord_id")
    private TransactionRecord transactionRecord;

    /**
     * Date when the transaction  has occurred
     */
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "transaction_date")
    private DateTime transactionDate;

    public boolean isRefunded() {
        return refunded;
    }

    public void setRefunded(boolean refunded) {
        this.refunded = refunded;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public BigDecimal getSrp() {
        return srp;
    }

    public void setSrp(BigDecimal srp) {
        this.srp = srp;
    }

    public BigDecimal getDp() {
        return dp;
    }

    public void setDp(BigDecimal dp) {
        this.dp = dp;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public TransactionRecord getTransactionRecord() {
        return transactionRecord;
    }

    public void setTransactionRecord(TransactionRecord transactionRecord) {
        this.transactionRecord = transactionRecord;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }


    public DateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(DateTime transactionDate) {
        this.transactionDate = transactionDate;
    }
}
