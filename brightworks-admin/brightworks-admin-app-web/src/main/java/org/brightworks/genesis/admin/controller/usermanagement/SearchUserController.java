package org.brightworks.genesis.admin.controller.usermanagement;

import org.brightworks.genesis.admin.controller.DataTablesResponse;
import org.brightworks.genesis.admin.dto.UserAccountDTO;
import org.brightworks.genesis.admin.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import static org.brightworks.genesis.admin.util.MavBuilder.render;

/**
 * Created by kyeljohndavid on 5/29/2015.
 */
@RestController
public class SearchUserController {


    @Autowired
    private UserAccountService userAccountService;

    @RequestMapping("/user/search")
    public ModelAndView showSearchUserPage(){
        return render("searchUser").toMav();
    }


    @RequestMapping(value = "/user/ajax/searchUser",method = RequestMethod.GET)
    public DataTablesResponse<UserAccountDTO> dataTableSearchProduct(@RequestParam (required=true) int sEcho,
                                                                     @RequestParam (required=true) int iDisplayStart,
                                                                     @RequestParam (required=true) int iDisplayLength,
                                                                     @RequestParam (required=true) int iColumns,
                                                                     @RequestParam (required=true) int iSortCol_0,
                                                                     @RequestParam (required=false)String sSortDir_0,
                                                                     @RequestParam (required=true) String sSearch ){


        int page = (int) Math.ceil(iDisplayStart/iDisplayLength);
        Page<UserAccountDTO> userAccountDTOs = userAccountService
            .search(sSearch, new PageRequest(page, iDisplayLength));

        return new DataTablesResponse<>(userAccountDTOs.getContent(),
            sEcho,
            userAccountDTOs.getTotalElements(),
            userAccountDTOs.getTotalElements());
    }
}
