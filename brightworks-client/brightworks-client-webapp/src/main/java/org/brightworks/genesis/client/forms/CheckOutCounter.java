package org.brightworks.genesis.client.forms;

import org.brightworks.genesis.inventory.dto.CheckOutItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kyel on 3/6/2015.
 * Scoped proxy session bean.
 * Checkout form will be persisted the session attribute
 *
 */
public class CheckOutCounter {

    private List<CheckOutItem> items;

    public List<CheckOutItem> getItems() {
        return items;
    }

    public void setItems(List<CheckOutItem> items) {
        this.items = items;
    }

    public List<CheckOutItem> add(CheckOutItem item){
        if(items == null){
            items = new ArrayList<>();
            items.add(item);
            return items;
        }else{
            if(items.contains(item)){
                int index = items.indexOf(item);
                CheckOutItem checkOutItem = items.get(index);
                int qty = checkOutItem.getQuantity()+1;
                checkOutItem.setQuantity(qty);
                items.remove(0);
                items.add(checkOutItem);
            }else{
                items.add(item);
            }
            return items;
        }
    }

    public void remove(int index){
        if(!items.isEmpty()){
            items.remove(index);
        }
    }
    public void clear(){
        items.clear();
    }

    public void add(List<CheckOutItem> checkOutItems){
        if(items != null){
            items.addAll(checkOutItems);
        }else{
            items = new ArrayList<>(checkOutItems);
        }
    }

    public Boolean isEmpty(){
        if(items == null){
            return  true;
        }else{
            return items.isEmpty();
        }
    }
}
