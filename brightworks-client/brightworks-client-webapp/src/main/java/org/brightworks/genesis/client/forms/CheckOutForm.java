package org.brightworks.genesis.client.forms;

import org.brightworks.genesis.inventory.dto.CheckOutItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kyel on 3/8/2015.
 */
public class CheckOutForm {

    private List<CheckOutItem> checkOutItemList = new ArrayList<CheckOutItem>();

    public CheckOutForm(){}

    public CheckOutForm(List<CheckOutItem> checkOutItemList){
        this.checkOutItemList = checkOutItemList;
    }

    public List<CheckOutItem> getItems() {
        clean();
        return checkOutItemList;
    }

    public void setItems(List<CheckOutItem> checkOutItemList) {
        this.checkOutItemList = checkOutItemList;
    }

    public List<CheckOutItem> clean(){
        /*
         *  Values with null id and null item code means they are deleted or non existent
         *  We only want to retrieve CheckOutItems that has values
         */
        for(CheckOutItem checkOutItem: checkOutItemList){
            if(checkOutItem.getId() == null && checkOutItem.getCode() == null){
                checkOutItemList.remove(checkOutItem);
            }
        }

        return checkOutItemList;
    }
}
