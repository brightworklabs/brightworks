package org.brightworks.genesis.admin.forms;


import org.brightworks.genesis.admin.dto.TenantInfoDTO;
import org.brightworks.genesis.admin.dto.UserAccountDTO;
import org.brightworks.genesis.admin.model.TenantInfo;
import org.brightworks.genesis.admin.role.AccountRole;

/**
 * Created by kyeljohndavid on 4/3/2015.
 */
public class UserAccountForm {

    private UserAccountDTO userAccount;

    private String retypedPassword;

    public UserAccountForm(UserAccountDTO userAccount) {
        this.userAccount = userAccount;
    }

    public UserAccountForm(){
        this.userAccount = new UserAccountDTO();
    }

    public String getUsername() {
        return userAccount.getUsername();
    }

    public void setUsername(String username) {
        this.userAccount.setUsername(username);
    }

    public String getPassword() {
        return userAccount.getPassword();
    }

    public void setPassword(String password) {
        userAccount.setPassword(password);
    }

    public String getFirstName() {
        return userAccount.getFirstName();
    }

    public void setFirstName(String firstName) {
        userAccount.setFirstName(firstName);
    }

    public String getLastName() {
        return userAccount.getLastName();
    }

    public void setLastName(String lastName) {
        this.userAccount.setLastName(lastName);
    }


    public AccountRole getAccountRole() {
        return userAccount.getAccountRole();
    }

    public void setAccountRole(AccountRole accountRole) {
        userAccount.setAccountRole(accountRole);
    }


    public void setTenantCode(String tenantCode){
        this.userAccount.setTenantCode(tenantCode);
    }

    public String getTenantCode(){
        return userAccount.getTenantCode();
    }

    public UserAccountDTO getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccountDTO userAccount) {
        this.userAccount = userAccount;
    }

    public String getRetypedPassword() {
        return retypedPassword;
    }

    public void setRetypedPassword(String retypedPassword) {
        this.retypedPassword = retypedPassword;
    }
}
