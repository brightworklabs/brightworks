<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<jsp:include page="includes/head.jsp" flush="true"/>
<body>
<jsp:include page="includes/page-header.jsp" flush="true" />
<!-- Content-->
<div class="container-fluid" id="container">

    <!-- Sidebar -->
    <jsp:include page="./includes/side-bar.jsp" flush="true" />

    <!-- Page content -->
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            New Tenant Registration
        </div><!--/.panel-heading-->
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6 centered">
                    <c:if test="${SUCCESS != null}">
                        <div class="alert alert-success" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only"></span> ${SUCCESS}
                        </div>
                    </c:if>
                    <c:if test="${ERROR != null}">
                        <div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only"></span> ${ERROR}
                        </div>
                    </c:if>
                    <form:form commandName="tenant" method="post" action="${pageContext.request.contextPath}/branch/save">
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Branch Name</label>
                            <form:input path="description" type="text" class="form-control" />
                        </div>
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Branch Code *alpha numeric only</label>
                            <form:input path="code" type="text" class="alpha-num form-control" />
                        </div>
                        <div class="col-md-12 form-group">
                            <button class="form-control btn btn-primary submit-button" type="submit">Save New Branch <i class="fa fa-check-square-o"></i></button>
                        </div>
                    </form:form>
                </div>
            </div><!--/.row-->
        </div><!--/.panel-body-->
    </div><!--panel-->
</div><!--/.container-->
<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- Login Page JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
<script>
    $(document).ready(function(){
        $('.alpha-num').bind('keypress', function (event) {
            var keyCode = event.keyCode || event.which
            // Don't validate the input if below arrow, delete and backspace keys were pressed
            if (keyCode == 8 || (keyCode >= 35 && keyCode <= 40)) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
                return;
            }

            var regex = new RegExp("^[a-zA-Z0-9]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        });
    });
</script>
</body>
</html>
