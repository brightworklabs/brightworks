package org.brightworks.genesis.inventory.model;

import org.brightworks.genesis.commons.model.JpaModel;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by kyel on 12/18/2014.
 */
@Entity
@Table(name = "REF_PRODUCT")
public class Product extends JpaModel{

    @Column(name= "NAME")
    private String name;

    @Column(name = "MANUFACTURER")
    private String manufacturer;

    /**
     * Product photos are optional
     */
    @OneToOne(optional = true)
    @JoinColumn(name="picture_id",nullable = true)
    private ProductPicture picture;

    @Embedded
    private Price price;

    /**
     * Product Type - use to check if an item
     * is a good that's goint to be sold or a service
     * that's going to be provided
     */
    @Column(name="PRODUCT_TYPE")
    @Enumerated(EnumType.STRING)
    private ProductType type;

    @Column(name = "CODE")
    private String code;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "STOCK")
    private int stock;

    /**
     * Location of the product in the warehouse
     */
    @Column(name = "LOCATION")
    private String location;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public BigDecimal getSrp(){
        return price.getSrp();
    }

    public BigDecimal getDp(){
        return price.getDp();
    }

    public BigDecimal getCost(){
        return price.getCost();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public ProductPicture getPicture() {
        return picture;
    }

    public void setPicture(ProductPicture picture) {
        this.picture = picture;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


}
