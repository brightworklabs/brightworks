/**
 * ModelMapper is an intelligent object mapping library.
 *
 * <p>
 * The principal public APIs in this package are:
 *
 *
 * <dt>{@link org.brightworks.genesis.client.controller.product}
 * <dd>The Package for the controllers that resides
 * in the url http://address.com/product/* together with
 * their Form backing Objects
 *
 * <dt>{@link org.brightworks.genesis.client.controller.ClientDashboardController}
 * <dd>The Dashboard. The First webpage the user will once he is aunthenticated
 */
package org.brightworks.genesis.client.controller;