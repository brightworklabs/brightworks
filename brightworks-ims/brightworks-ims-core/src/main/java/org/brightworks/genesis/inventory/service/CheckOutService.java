package org.brightworks.genesis.inventory.service;

import org.brightworks.genesis.inventory.dto.CheckOutItem;
import org.brightworks.genesis.inventory.dto.TransactionRecordDTO;
import org.brightworks.genesis.inventory.model.transaction.TransactionRecord;

import java.util.List;

/**
 * Created by kyeljohndavid on 3/21/2015.
 */
public interface CheckOutService {

    TransactionRecordDTO checkOut(List<CheckOutItem> checkOutItems);
}
