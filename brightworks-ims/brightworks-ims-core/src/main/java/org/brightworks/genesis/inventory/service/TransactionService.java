package org.brightworks.genesis.inventory.service;

import org.brightworks.genesis.inventory.dto.TransactionRecordDTO;
import org.joda.time.DateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * Created by kyeljohndavid on 3/30/2015.
 */
public interface TransactionService {

    TransactionRecordDTO findTransactionRecord(String transactionNumber);

    TransactionRecordDTO findTransactionRecord(Long transactionId);

    Page<TransactionRecordDTO> filterByDateRange(DateTime from, DateTime to,Pageable page);
}
