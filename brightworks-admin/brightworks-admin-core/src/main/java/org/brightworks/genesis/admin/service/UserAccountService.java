package org.brightworks.genesis.admin.service;

import org.brightworks.genesis.admin.dto.UserAccountDTO;
import org.brightworks.genesis.admin.model.UserAccount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by kyel on 10/5/2014.
 */
public interface UserAccountService {

    UserAccountDTO saveAccountInformation(UserAccountDTO account);

    UserAccount findByUsername(String username);

    boolean exists(String username);

    UserAccountDTO findById(Long id);

    Page<UserAccountDTO> search(String term,Pageable pageable);

    List<UserAccountDTO> findByBranchCode(String code);
}
