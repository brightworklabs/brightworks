<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<jsp:include page="includes/head.jsp" flush="true"/>
<body>
<jsp:include page="includes/page-header.jsp" flush="true" />

<!-- Content-->
<div class="container-fluid" id="container">
    <!-- Sidebar -->
    <jsp:include page="./includes/side-bar.jsp" flush="true" />
    <!-- Page content -->
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            New User Registration
        </div><!--/.panel-heading-->
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6 centered"><%--
                    <ul class="list-group">
                    <c:forEach var="error" items="${result}" >
                            <li class="list-group-item list-group-item-danger">${error}</li>
                    </c:forEach>
                    </ul>--%>
                    <form:form commandName="account" method="post" action="${pageContext.request.contextPath}/user/save">
                        <!-- Username -->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Username</label>
                            <form:input path="username" type="text" cssErrorClass="input-error form-control" class="form-control" />
                            <form:errors path="username" cssClass="label label-danger" />
                        </div>
                        <!-- Password -->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Password</label>
                            <form:password path="password" cssErrorClass="input-error form-control" class="form-control"/>
                            <form:errors path="password" cssClass="label label-danger" />
                        </div>
                        <!-- Password -->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Retyped Password</label>
                            <form:password path="retypedPassword" cssErrorClass="input-error form-control" class="form-control"/>
                            <form:errors path="retypedPassword" cssClass="label label-danger" />
                        </div>

                        <!-- First Name -->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">First Name</label>
                            <form:input path="firstName" type="text" cssErrorClass="error" class="form-control"/>
                        </div>

                        <!-- First Name -->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Last Name</label>
                            <form:input path="lastName" type="text" class="form-control"/>
                        </div>

                        <!-- User Role -->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">User Role</label>
                            <form:select path="accountRole" class="form-control">
       <%--                         <c:forEach var="role" items="${ACCOUNT_ROLES}">
                                    <option value="${role.code}">${role}</option>
                                </c:forEach>--%>
                                <form:options items="${ACCOUNT_ROLES}" itemLabel="name"/>
                            </form:select>
                        </div>

                        <!-- Branch Designation -->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Designated Branch</label>
                            <form:select path="tenantCode" class="form-control">
                                <c:forEach var="tenant" items="${TENANTS}">
                                    <option value="${tenant.tenantCode}">${tenant.tenantDescription}</option>
                                </c:forEach>
                            </form:select>
                        </div>
                        <!-- Button -->
                        <div class="col-md-12 form-group">
                            <button class="form-control btn btn-primary submit-button" type="submit">Save New User <i class="fa fa-check-square-o"></i></button>
                        </div>
                    </form:form>
                </div>
            </div><!--/.row-->
        </div><!--/.panel-body-->
    </div><!--panel-->
</div><!--/.container-->

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

<!-- Login Page JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
</body>

</html>
