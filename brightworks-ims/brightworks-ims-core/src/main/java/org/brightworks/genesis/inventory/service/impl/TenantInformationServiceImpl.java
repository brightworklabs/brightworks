package org.brightworks.genesis.inventory.service.impl;

import org.brightworks.genesis.inventory.dto.TenantInfoResponse;
import org.brightworks.genesis.inventory.service.TenantInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

/**
 * Created by kyeljohndavid on 5/17/2015.
 */
@Service
public class TenantInformationServiceImpl implements TenantInformationService {

    private static String HOST = "localhost";

    private static String PORT = "8081";

    private static String API_URI = "/api/branches";

    private static String SCHEME = "http://";

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<TenantInfoResponse> getAllTenants() {
        TenantInfoResponse[] tenants = restTemplate.getForObject(SCHEME+HOST+":"+PORT+API_URI,TenantInfoResponse[].class);
        return Arrays.asList(tenants);
    }
}
