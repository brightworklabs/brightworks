<%--
  Created by IntelliJ IDEA.
  User: Kurt
  Date: 2/19/2015
  Time: 5:11 PM
  To change this template use File | Settings | File Templates.
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<jsp:include page="./includes/head.jsp" flush="true"/>
<body>
<!-- Header -->
<jsp:include page="./includes/page-header.jsp" flush="true" />
<!-- Content-->
<div class="container-fluid" id="container">
    <!-- Sidebar -->
    <jsp:include page="./includes/side-bar.jsp" flush="true" />
    <!-- Page content -->
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            Checkout
        </div>
        <!--TODO: UPDATE FIELDS -->
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 text-right">

                    <h1 style="display:inline" class="text-primary">Total</h1>
                    <span>&nbsp;</span>
                    <span>&nbsp;</span>
                    <span>&nbsp;</span>
                    <span>&nbsp;</span>
                    <span class="total-peso">PHP</span>
                    <span>&nbsp;</span>
                    <span>&nbsp;</span>
                    <span class="total" id="total">0.00</span>

                </div>
            </div><!--/.row-->

            <hr>

            <form:form commandName="checkOutForm" id="checkoutForm" action="${pageContext.request.contextPath}/product/checkout" method="POST">
                <c:forEach var="item" varStatus="status" items="${checkOutForm.items}">
                    <div class="row" id="checkout-items-list">
                        <input type="hidden" class="itemCode form-control" name="items[${status.index}].id"
                               value = "${item.id}">
                        <input type="hidden" class="itemCode form-control" name="items[${status.index}].cost"
                               value = "${item.cost}">
                        <div class="well" id="checkout-item">
                            
                                <a href="${pageContext.request.contextPath}/ajax/product/remove/${status.index}"
                                   class="remove-item close remove-btn" data-dismiss="well">&times;</a>
                            
                                <fieldset>
                                    <!-- TODO: Replace with real index -->
                                    <input type="hidden" class="itemCode form-control" name="items[${status.index}].id" placeholder="Item Code">
                                    <div class="form-group col-md-2 col-xs-12">
                                        <label class="control-label text-primary" for="itemCode">
                                            Item Code
                                        </label>
                                        <input type="text" class="itemCode form-control" name="items[${status.index}].code" id="itemCode" placeholder="Item Code"
                                            value = "${item.code}">
                                    </div>
                                    <div class="form-group col-md-2 col-xs-12">
                                        <label class="control-label text-primary" for="itemName">
                                            Item Name
                                        </label>
                                        <input type="text" class="itemName form-control" name="items[${status.index}].name" id="itemName" placeholder="Item Name"
                                            value="${item.name}">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label class="control-label text-primary" for="transactionType">
                                            Transaction Type
                                        </label>
                                        <select name="items[${status.index}].priceIdentifier" class="transactionType form-control" id="transactionType">
                                            <option value="srp">Suggested Retail Price</option>
                                            <option value="dp">Dealer's Price</option>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-md-2 col-xs-12">
                                        <label class="control-label text-primary" for="itemPrice">
                                            Price
                                        </label>
                                        <input type="text" class="item-price form-control"
                                               value="${item.srp}"
                                               id="itemPrice" placeholder="0.00" disabled>
                                        <input type="hidden" name="items[${status.index}].dp"
                                            value="${item.dp}"
                                            class="item-price-dp"
                                            />
                                        <input type="hidden"
                                               name="items[${status.index}].srp"
                                               class="item-price-srp"
                                               value="${item.srp}"/>
                                    </div>
                                    <div class="form-group col-md-2 col-xs-12">
                                        <label class="control-label text-primary" for="itemQuantity">
                                            Quantity
                                        </label>
                                        <input type="number" name="items[${status.index}].quantity"
                                               class="item-Quantity form-control" placeholder="0"
                                            value="${item.quantity}" min="1">
                                    </div>
                                    <div class="form-group col-md-2 col-xs-12">
                                        <label class="control-label text-primary" for="subtotal">
                                            Subtotal
                                        </label>
                                        <!--
                                            Default Price will be the SRP.
                                            Use Javascript to change value dynamically
                                            when the price is set to another type of
                                            price identifer
                                        -->
                                        <input type="text" class="subtotal form-control" name="items[${status.index}].total" id="subtotal" value="${item.srp * item.quantity}" placeholder="0.00" disabled>
                                    </div>
                                    
                                    <!-- DELETE BUTTON --
                                    <div class="col-md-1 col-xs-3">
                                        <a class="btn btn-danger remove-btn" href="#">
                                            <i class="fa fa-trash-o fa-lg"></i>
                                        </a>
                                    </div>
                                    -->
                                </fieldset>
                            </div><!--/.well-->
                    </div><!--/.row-->
                </c:forEach>
                <!-- ADD ITEMS TO CHECKOUT BUTTON
                <div class="row">
                    <div class="add-btn-container centered">
                        <a class="btn btn-success" href="#" id="addButton">
                            <i class="fa fa-plus fa-sm"></i>
                        </a>
                    </div>
                </div>
                -->
            

                <div class="row">
                    <div class="confirm-checkout pull-right">
                        <a href="#confirmModal" class="btn btn-primary" id="confirmButton" data-toggle="modal" role="button">Confirm Transaction <i class="fa fa-check-square-o fa-sm icon-right"></i></a>
                    </div>
                </div><!--/.row-->


            </form:form>

        </div><!--/.panel-body-->
    </div><!--/.panel-->

     <!-- Modal -->
    <div class="modal fade" id="confirmModal">
        <div class="modal-dialog modal-vertical-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">&times;</button>
                    <h4><strong class="text-primary">Checkout</strong></h4>
                </div>

                <div class="modal-body">
                    Are you sure you want to record this transaction?
                </div>

                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>

                    <button class="btn btn-primary" id ="submit-checkout-form" data-dismiss="modal">Confirm <i class="fa fa-check-square-o fa-sm icon-right"></i></button>
                </div>
            </div>
        </div>
    </div><!--/.modal-->

</div><!--/.container-->
<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.11.0.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- Login Page JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
<script>
    $(function() {
        $("#submit-checkout-form").click(function(e){
          $("#checkoutForm").submit();
        });


        $(".remove-item").click(function(e){
            e.preventDefault();
            $.get($(this).attr("href"))
        });

    });
    
    //Compute Grand Total after Page Load
    computeGrandTotal();

</script>
</body>
</html>
