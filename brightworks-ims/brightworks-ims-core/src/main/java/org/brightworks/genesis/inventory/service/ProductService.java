package org.brightworks.genesis.inventory.service;

import org.brightworks.genesis.inventory.dto.ProductDTO;
import org.brightworks.genesis.inventory.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by kyel on 12/22/2014.
 */
public interface ProductService {

    Product saveProduct(ProductDTO productDTO) throws ProductExistsException;

    ProductDTO findByCode(String code);

    ProductDTO findById(Long id);

    List<ProductDTO> findProductsByCode(String code);

    List<ProductDTO> findProductsByName(String name);

    List<Product> findByManufacturer(String manufacturer);

    List<ProductDTO> searchsProductByName(String name,Pageable pageable);

    Page<ProductDTO> search(String term,Pageable pageable);
}
