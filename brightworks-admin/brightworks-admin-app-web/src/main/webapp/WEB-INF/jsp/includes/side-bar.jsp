    <!-- Sidebar -->
    <div id="sidebar-container">
    <ul class="sidebar-nav">
        <li><a href="${pageContext.request.contextPath}/user/search"><i class="sidebar-nav-icon fa fa-users"></i>User List</a></li>
        <li><a href="${pageContext.request.contextPath}/branch/all"><i class="sidebar-nav-icon fa fa-building-o"></i>Branch List</a></li>
        <li><a href="${pageContext.request.contextPath}/user/add"><i class="sidebar-nav-icon fa fa-user-plus"></i>Add User</a></li>
        <li><a href="${pageContext.request.contextPath}/branch/add"><i class="sidebar-nav-icon fa fa-map-marker"></i>Add Branch</a></li>
        
    </ul>
    <ul class="sidebar-nav bottom-sidebar-nav">
        <li><a href="${pageContext.request.contextPath}/j_spring_security_logout"><i class="sidebar-nav-icon fa fa-power-off"></i>Log Out</a></li>
    </ul>

</div>
