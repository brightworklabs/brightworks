<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="includes/head.jsp" flush="true"/>
<body>
<jsp:include page="includes/page-header.jsp" flush="true" />

<!-- Content-->
<div class="container-fluid" id="container">

    <!-- Sidebar -->
    <jsp:include page="./includes/side-bar.jsp" flush="true" />

    <!-- Page content -->
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3 no-padding">
                    Product Page
                </div>

                <div class="col-xs-1 pull-right text-right no-padding">
                    <a href="#" data-toggle="dropdown" class="product-panel-dropdown"><i class="fa fa-cog"></i></a>

                    <ul class="product-panel-dropdown-menu dropdown-menu" role="menu">
                        <li><a href="${pageContext.request.contextPath}/product/edit/${product.id}"><i class="fa fa-pencil fa-sm icon-left"></i> Edit Product Information</a></li>
                        <li><a id = "checkout" href="${pageContext.request.contextPath}/ajax/product/checkout?itemCode=${product.code}"><i class="fa fa-shopping-cart fa-sm icon-left"></i> Add Item To Checkout</a></li>
                    </ul>

                </div>

            </div><!--/.row-->

        </div><!--/.panel-heading-->

        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="product-image">
                        <c:choose>
                            <c:when test="${!product.noImage}">
                               <img id="product-image" src="${pageContext.request.contextPath}/product/image/${product.pictureId}"/>
                            </c:when>
                            <c:otherwise>
                                <img id="product-image" src="${pageContext.request.contextPath}/resources/img/image-placeholder.png"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="product-title text-primary"><h1>${product.name}</h1></div>
                    <div class="product-number text-info"><h4>#${product.code}</h4></div>

                    <hr>

                    <div class="product-desc"><p>${product.description}</p></div>
                </div>
            </div><!--/.row-->

            <hr>

            <div class="row">
                <!-- SRP Panel -->
                <div class="col-md-3">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-2 no-padding">
                                    <i class="fa fa-money fa-3x"></i>
                                </div>
                                <div class="col-xs-10 text-right no-padding">
                                    <h3 class="no-margin-top">PHP
                                        <fmt:formatNumber type="number"
                                        maxFractionDigits="2"
                                        pattern="#,##0.00"
                                        value="${product.srp}" />
                                    </h3>
                                    Sugested Retail Price
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Dealer's Price Panel -->
                <div class="col-md-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-2 no-padding">
                                    <i class="fa fa-truck fa-3x"></i>
                                </div>
                                <div class="col-xs-10 text-right no-padding">
                                    <h3 class="no-margin-top">PHP
                                        <fmt:formatNumber type="number"
                                        maxFractionDigits="2"
                                        pattern="#,##0.00"
                                        value="${product.dp}"/>
                                    </h3>
                                    Dealer's Price
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <sec:authorize access="hasRole('ROLE_ADMIN')">
                <!-- Item Cost Panel -->
                    <div class="col-md-3">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-2 no-padding">
                                        <i class="fa fa-barcode fa-3x"></i>
                                    </div>
                                    <div class="col-xs-10 text-right no-padding">
                                        <h3 class="no-margin-top">PHP
                                            <fmt:formatNumber type="number"
                                                maxFractionDigits="2"
                                                pattern="#,##0.00"
                                                value="${product.cost}"/></h3>
                                        Item Cost
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </sec:authorize>
                <!-- Stocks Remaining Panel -->
                <div class="col-md-3">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-2 no-padding">
                                    <i class="fa fa-cubes fa-3x"></i>
                                </div>

                                <div class="col-xs-10 text-right no-padding">
                                    <h3 class="no-margin-top">${product.stock}</h3>
                                    Stocks Remaining
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.row-->
        </div><!--/.panel-body-->
    </div><!--/.panel-->
    <!-- Button trigger modal -->

    <!-- Success Modal -->
    <div class="modal fade" id="addedToCheckOutModal">
        <div class="modal-dialog modal-vertical-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4><strong class="text-primary">Product Page</strong></h4>
                </div>

                <div class="modal-body">
                    Item Successfully Added to Checkout!
                </div>

                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>

                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/product/checkout">Proceed To Checkout <i class="fa fa-shopping-cart fa-sm icon-right"></i></a>
                </div>
            </div>
        </div>
    </div><!--/.modal-->

    <!-- Error Modal -->
    <div class="modal fade" id="noStockModal">
        <div class="modal-dialog modal-vertical-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4><strong class="text-primary">Product Page</strong></h4>
                </div>

                <div class="modal-body">
                    Item Out of Stock!
                </div>

                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>

                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/product/search">Browse Inventory <i class="fa fa-search fa-sm icon-right"></i></a>
                </div>
            </div>
        </div>
    </div><!--/.modal-->

</div><!--/.container-->

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- Login Page JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
<script>
    $("#checkout").click(function(e){
        e.preventDefault();
        var href = $(this).attr("href")
        $.ajax({
            url: href,
            success: function(e){
                $("#addedToCheckOutModal").modal("show")
            },
            error:function(e){
                $("#noStockModal").modal("show")
            }
        })
    })
</script>
</body>

</html>
