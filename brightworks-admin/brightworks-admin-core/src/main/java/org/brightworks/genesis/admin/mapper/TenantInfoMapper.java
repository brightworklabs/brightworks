package org.brightworks.genesis.admin.mapper;

import org.brightworks.genesis.admin.dto.TenantInfoDTO;
import org.brightworks.genesis.admin.model.TenantInfo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * Created by kyeljohndavid on 4/19/2015.
 */
@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface TenantInfoMapper {

    TenantInfoMapper INSTANCE = Mappers.getMapper(TenantInfoMapper.class);

    @Mappings(value = {
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "tenantCode",target = "tenantCode"),
        @Mapping(source = "tenantDescription",target = "tenantDescription")
    })
    TenantInfoDTO toDto(TenantInfo tenantInfo);

    List<TenantInfoDTO> toTenantInfoDtos(List<TenantInfo> tenantInfoList);
}
