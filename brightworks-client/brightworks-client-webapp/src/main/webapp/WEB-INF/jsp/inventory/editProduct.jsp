<%--
  Created by IntelliJ IDEA.
  User: Kurt
  Date: 11/27/2014
  Time: 5:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
    
<html>
<jsp:include page="../includes/head.jsp" flush="true"/>
<body>
<!-- Header -->
<jsp:include page="../includes/page-header.jsp" flush="true" />

<!-- Content-->
<div class="container-fluid" id="container">

    <!-- Sidebar -->
    <jsp:include page="../includes/side-bar.jsp" flush="true" />

    <!-- Page content -->
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            Edit Products
        </div><!--/.panel-heading-->
        
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6 centered">
                    <form>
                        <!-- Image Input -->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Product Image</label>
                            <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled">
                                <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                        <span class="glyphicon glyphicon-folder-open"></span>
                                        <span class="image-preview-input-title">Browse</span>
                                        <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/>
                                    </div>
                                </span>
                            </div>
                        </div>

                        <!-- Product Number -->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Product Number</label><input type="text" class="form-control">

                        </div>

                        <!-- Product Name-->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Product Name</label><input type="text" class="form-control">
                        </div>

                        <!-- Product Description -->
                        <div class="col-xs-12 form-group">
                            <label class="control-label">Product Description</label>
                            <textarea type="text" class="form-control"></textarea>

                        </div>

                        <!-- Price, Cost and  Quantity -->
                        <div class="col-xs-4 form-group required">
                            <label class="control-label">Item Price</label>
                            <input class="form-control" size="4" type="text">
                        </div>
                        <div class="col-xs-4 form-group required">
                            <label class="control-label">Item Cost</label>
                            <input class="form-control" type="text">
                        </div>
                        <div class="col-xs-4 form-group required">
                            <label class="control-label">Quantity</label>
                            <input class="form-control" type="text">
                        </div>

                        <!-- Button -->
                        <div class="col-md-12 form-group">
                            <button class="form-control btn btn-primary submit-button" type="submit">Confirm Changes »</button>

                        </div>

                    </form>

                </div>

            </div><!--/.row-->

        </div><!--/.panel-body-->
   
    </div><!--panel-->
</div><!--/.container-->

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

<!-- Login Page JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
</body>
</html>
