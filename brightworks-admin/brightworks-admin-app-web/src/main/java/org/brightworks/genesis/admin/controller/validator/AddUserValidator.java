package org.brightworks.genesis.admin.controller.validator;

import org.brightworks.genesis.admin.forms.UserAccountForm;
import org.brightworks.genesis.admin.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by kyeljohndavid on 4/21/2015.
 */
@Component
public class AddUserValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return UserAccountForm.class.equals(aClass);
    }

    @Autowired
    private UserAccountService userAccountService;

    @Override
    public void validate(Object o, Errors errors) {
        UserAccountForm userAccountForm = (UserAccountForm) o;

        if (userAccountForm.getUserAccount().getId() == null && userAccountForm.getPassword() == null) {
            errors.rejectValue("Password", "password.empty", "Password Must Not Be Empty");
        }

        if (userAccountForm.getUsername() == null) {
            errors.rejectValue("username", "username.empty", "User Name Must Not Be Empty");
        }

        if (userAccountForm.getUserAccount().getId() == null && userAccountForm.getRetypedPassword() == null) {
            errors.rejectValue("retypedPassword", "Retyped Password Must not be empty");
        }

        if (userAccountForm.getUsername() != null && userAccountForm.getUsername().length() < 6) {
            errors.rejectValue("username", "username.less.than.six", "User Name Must Not Be Less Than 6 Characters");
        }
        if (userAccountForm.getUserAccount().getId() == null && userAccountService.exists(userAccountForm.getUsername())) {
            errors.rejectValue("username", "username.exists", "User Name Already Exists");
        }

        if (userAccountForm.getPassword() != null && userAccountForm.getPassword().length() < 6) {
            errors.rejectValue("password", "password.less.than.six", "Password Must Not Be Less Than 6 Characters");
        }

        if (userAccountForm.getPassword() != null && userAccountForm.getRetypedPassword() != null) {
            if (!userAccountForm.getPassword().equals(userAccountForm.getRetypedPassword())) {
                errors.rejectValue("password", "password.not.match", "Password Does Not Match");
                errors.rejectValue("retypedPassword", "match.error", "");
            }
        }
    }
}
