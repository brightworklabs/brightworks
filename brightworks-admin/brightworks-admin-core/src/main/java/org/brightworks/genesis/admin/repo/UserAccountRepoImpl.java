package org.brightworks.genesis.admin.repo;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import org.brightworks.genesis.admin.dto.UserAccountDTO;
import org.brightworks.genesis.admin.model.QUserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by kyeljohndavid on 5/29/2015.
 */
public class UserAccountRepoImpl implements UserAccountRepoCustom {

    @Autowired
    private EntityManager em;

    @Override
    public Page<UserAccountDTO> search(String term, Pageable pageable) {
        JPAQuery query = new JPAQuery(em);
        QUserAccount userAccount = QUserAccount.userAccount;

        List<UserAccountDTO> result = query
            .from(userAccount)
            .where(userAccount.username.containsIgnoreCase(term)
                .or(userAccount.name.givenName.containsIgnoreCase(term))
                .or(userAccount.name.lastName.containsIgnoreCase(term)))
            .list(ConstructorExpression.create(UserAccountDTO.class,userAccount.id,userAccount.username,
                userAccount.name.givenName,userAccount.name.lastName,userAccount.role,userAccount.tenantInfo.tenantDescription,
                userAccount.tenantInfo.tenantCode));

        return new PageImpl<UserAccountDTO>(result,pageable,query.count());
    }
}
