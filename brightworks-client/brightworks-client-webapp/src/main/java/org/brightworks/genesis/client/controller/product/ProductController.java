package org.brightworks.genesis.client.controller.product;

import org.brightworks.genesis.client.forms.ProductForm;
import org.brightworks.genesis.client.util.MavBuilder;
import org.brightworks.genesis.inventory.dto.ProductDTO;
import org.brightworks.genesis.inventory.model.Product;
import org.brightworks.genesis.inventory.service.ProductExistsException;
import org.brightworks.genesis.inventory.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Created by kyel on 12/5/2014.
 */
@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    private Logger logger = LoggerFactory.getLogger(ProductController.class);

    private static String ROOT_VIEW_URI = "inventory";

    private static String ADD_PRODUCT_VIEW = ROOT_VIEW_URI +"/addProduct";

    private static String EDIT_PRODUCT_VIEW = ROOT_VIEW_URI + "/addProduct";

    private static String SEARCH_PRODUCT_VIEW = ROOT_VIEW_URI + "/search";


    private static String FORM_OBJECT = "product";


    @RequestMapping("/add")
    public ModelAndView addProductForm(){
        return MavBuilder
                .render(ADD_PRODUCT_VIEW)
                .addAttr(FORM_OBJECT, new ProductForm())
                .toMav();
    }

    @RequestMapping(value="/save",method = RequestMethod.POST,consumes="multipart/form-data")
    public ModelAndView saveProductForm(@ModelAttribute("product") ProductForm productForm,BindingResult result){
        Product product = null;
        try {
            product = productService.saveProduct(productForm.getProductDTO());
        } catch (ProductExistsException e) {
            return MavBuilder
                    .render(ADD_PRODUCT_VIEW)
                    .addAttr("error","Product with the given item code already exists.")
                    .addAttr(FORM_OBJECT, productForm)
                    .toMav();
        }
        return new ModelAndView(new RedirectView("/product/view/"+product.getCode()));
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView editProductForm(@PathVariable("id")Long id){
        ProductDTO productDTO = productService.findById(id);
        return MavBuilder
            .render(EDIT_PRODUCT_VIEW)
            .addAttr(FORM_OBJECT, new ProductForm(productDTO))
            .toMav();
    }


    @RequestMapping(value = "/view/{code}")
    public ModelAndView viewProduct(@PathVariable("code")String code) {
        ProductDTO product = productService.findByCode(code);
        return MavBuilder
                .render("viewProduct")
                .addAttr(FORM_OBJECT,product)
                .toMav();
    }

    @RequestMapping("/search")
    public String showSearchProductForm(){
        return SEARCH_PRODUCT_VIEW;
    }
}
