package org.brightworks.genesis.commons.model;

import javax.persistence.*;

/**
 * Created by kyel on 10/26/2014.
 */
@MappedSuperclass
public abstract class BaseAccountModel extends JpaModel {

    @Column(name = "USERNAME",unique = true)
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "IS_ACTIVE")
    protected Boolean active;

    @Embedded
    private Name name;

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setGivenName(String givenName){
        name.setGivenName(givenName);
    }

    public void setLastName(String lastName){
        name.setLastName(lastName);
    }

    public String getGivenName(){
        return name.getGivenName();
    }

    public String getLastName(){
        return name.getLastName();
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    @Embeddable
    public static class Name  {

        @Column(name = "GIVEN_NAME")
        private String givenName;

        @Column(name = "MIDDLE_NAME")
        private String middleName;

        @Column(name = "LAST_NAME")
        private String lastName;


        public String getGivenName() {
            return givenName;
        }

        public void setGivenName(String givenName) {
            this.givenName = givenName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }
    }
}
