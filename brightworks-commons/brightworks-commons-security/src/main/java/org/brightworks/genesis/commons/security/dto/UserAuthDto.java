package org.brightworks.genesis.commons.security.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kyeljohndavid on 10/29/14.
 */
public class UserAuthDto {

    private String username;

    private String password;

    private List<String> roles = new ArrayList<String>();

    private String tenantCode;

    public UserAuthDto(String username,
                           String password,
                           List<String> roles,
                           String tenantCode){
        this.username=username;
        this.password=password;
        this.roles=roles;
        this.tenantCode=tenantCode;
    }

    public UserAuthDto(String username,
                       String password){
        this.username=username;
        this.password=password;
    }

    public UserAuthDto(){

    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public List<String> getRoles() {
        return roles;
    }

    public String getTenantCode() {
        return tenantCode;
    }
}
