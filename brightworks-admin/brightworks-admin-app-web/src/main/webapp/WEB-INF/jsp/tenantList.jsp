<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <jsp:include page="./includes/head.jsp" flush="true"/>
    <body>
    <!-- Header -->
    <jsp:include page="./includes/page-header.jsp" flush="true" />

    <!-- Content-->
    <div class="container-fluid" id="container">

        <!-- Sidebar -->
        <jsp:include page="./includes/side-bar.jsp" flush="true" />

        <!-- Page content -->
        <div class="panel panel-primary content-panel">
            <div class="panel-heading">
                Branch List
            </div><!--/.panel-heading-->

            <!-- Search Form -->
            <div class="panel-body">
                <!-- Results -->
                <div class="col-md-8 centered">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td><h4 class="text-primary"><strong>Branch Code</strong></h4></td>
                                <td><h4 class="text-primary"><strong>Location</strong></h4></td>
                            </tr>
                        </thead>

                        <tbody>
                        <c:forEach var="tenant" varStatus="status" items="${tenants}">
                            <tr>
                                <td><a href="/branch/view/${tenant.tenantCode}">${tenant.tenantCode}</a></td>
                                <td>${tenant.tenantDescription}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div><!--/.row-->
            </div><!--/.panel-body

        </div><!--/.panel-->


    </div><!--/.container-->

    <!-- jQuery -->
    <script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <!-- Main JavaScript -->
    <script src="${pageContext.request.contextPath}/resources/js/main.js"></script>

    </body>
</html>