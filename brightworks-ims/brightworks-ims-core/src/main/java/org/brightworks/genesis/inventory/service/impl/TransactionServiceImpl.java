package org.brightworks.genesis.inventory.service.impl;

import org.brightworks.genesis.inventory.dto.LineItemDTO;
import org.brightworks.genesis.inventory.dto.TransactionRecordDTO;
import org.brightworks.genesis.inventory.mapper.LineItemMapper;
import org.brightworks.genesis.inventory.mapper.TransactionRecordMapper;
import org.brightworks.genesis.inventory.model.transaction.TransactionRecord;
import org.brightworks.genesis.inventory.repo.TransactionRecordRepo;
import org.brightworks.genesis.inventory.service.TransactionService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kyeljohndavid on 3/30/2015.
 */
@Service
@Transactional
public class TransactionServiceImpl implements TransactionService{

    @Autowired
    private TransactionRecordRepo transactionRecordRepo;

    @Override
    public TransactionRecordDTO findTransactionRecord(String transactionNumber) {
        TransactionRecord txnRecord = transactionRecordRepo.findByTransactionNumber(transactionNumber);
        return buildTransactionRecordDTO(txnRecord);
    }

    @Override
    public TransactionRecordDTO findTransactionRecord(Long transactionId) {
        TransactionRecord txnRecord = transactionRecordRepo.findOne(transactionId);
        return  buildTransactionRecordDTO(txnRecord);
    }

    @Override
    public Page<TransactionRecordDTO> filterByDateRange(DateTime from, DateTime to, Pageable page) {
        Page<TransactionRecord> records  = transactionRecordRepo.filterByDateRange(from,to,page);
        return new PageImpl<TransactionRecordDTO>(buildTransactionRecordDTO(records.getContent()),page,records.getTotalElements());
    }


    private TransactionRecordDTO buildTransactionRecordDTO(TransactionRecord txnRecord) {
        TransactionRecordDTO txnRecordDTO = TransactionRecordMapper.INSTANCE.toTransactionRecord(txnRecord);
        List<LineItemDTO> itemDTOs = LineItemMapper.INSTANCE.toLineItemDtos(txnRecord.getLineItems());
        txnRecordDTO.setLineItems(itemDTOs);
        return txnRecordDTO;
    }

    private List<TransactionRecordDTO> buildTransactionRecordDTO(List<TransactionRecord> records){
        List<TransactionRecordDTO>  recordDTOs = new ArrayList<TransactionRecordDTO>();
        for(TransactionRecord record : records){
            recordDTOs.add(buildTransactionRecordDTO(record));
        }
        return  recordDTOs;
    }
}
