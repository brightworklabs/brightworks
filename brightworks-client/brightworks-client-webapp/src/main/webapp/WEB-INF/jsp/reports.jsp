<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp" flush="true"/>
<body>
<jsp:include page="includes/page-header.jsp" flush="true" />

<!-- Content-->
<div class="container-fluid" id="container">

    <!-- Sidebar -->
    <jsp:include page="includes/side-bar.jsp" flush="true" />

    <!-- Page content -->
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            Reports
        </div><!--/.panel-heading-->

        <div class="panel-body" style="min-height: 200px;">

            <div class="col-md-6 centered">
                <div class="row">
                    <div class="col-md-12 col-sm-12 form-group">
                        <h3 class="text-primary">Select Report</h3>
                        <select id="report-option" class="form-control">
                            <option value="${pageContext.request.contextPath}/reports/sales">Sales Report</option>
                            <option value="${pageContext.request.contextPath}/reports/profit">Profit Report</option>
                        </select>
                    </div>

                    <div class="col-md-12 col-sm-12 form-group">
                        <h4 class="text-primary">Branch</h4>
                        <select id="tenant" class="form-control">
                                <option value="ALL">All</option>
                            <c:forEach items="${tenants}" var="tenant">
                                <option value="${tenant.tenantCode}">${tenant.tenantDescription}</option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="col-md-12 col-sm-12 form-group">
                        <h3 class="text-primary">Filter Report</h3>
                        <div class="col-md-6 col-sm-12 no-padding-left"><label>Date From</label><input type="date" class="form-control" id="dateFrom"></div>
                        <div class="col-md-6 col-sm-12 no-padding-right"><label>Date To</label><input type="date" class="form-control" id="dateTo"></div>
                    </div>

                    <div class="col-md-12 col-sm-12">
                        <button id="generate_report" class="form-control btn btn-primary generate-btn" type="submit">Generate Report<i class="fa fa-file-o icon-right"></i></button>
                    </div>
                </div><!--/.row-->
            </div>

        </div><!--/.panel-body-->
    </div><!--/.panel-->

</div><!--/.container-->
<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- Main JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
<script>
    $(function(){
        $("#generate_report").click(function(e){
            var dateTo = $("#dateTo").val();
            var dateFrom = $("#dateFrom").val();
            var tenant = $("#tenant").val();
            var contextPath = "${pageContext.request.contextPath}"+$("#report-option").val()+"?dateTo="+dateTo+"&dateFrom="+dateFrom+"&tenant="+tenant;
            window.location.replace(contextPath);
        });
    })
</script>
</body>

</html>
