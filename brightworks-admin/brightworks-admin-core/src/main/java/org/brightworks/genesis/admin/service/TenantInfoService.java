package org.brightworks.genesis.admin.service;

import org.brightworks.genesis.admin.dto.TenantInfoDTO;

import java.util.List;

/**
 * Created by kyeljohndavid on 4/17/2015.
 */
public interface TenantInfoService {

    TenantInfoDTO save(TenantInfoDTO tenantInfo);

    TenantInfoDTO getTenantInfo(String code);

    boolean exists(String code);

    List<TenantInfoDTO> getAll();

}
