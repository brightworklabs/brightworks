# Brightworks - Genesis 


# Repository Information

* An Inventory and Order Management System for Kenlex Telecoms Developed by the Team Brightworks(Kyel John M. David, and Nathaniel Kurt S. Lagare)
* version 1.0.0-SNAPSHOT



# How do I get the application running? #

NOTE: The configuration files configured here are for development environments. Please
change them accordingly once deployed on the development servers.


The configuration files is written as XML which can be found under the WEB-INF/context directory for both 
```
#!xml

brightworks-admin-webapp
```
and 
```
#!xml

brightworks-client-webapp. 
```
Their respective .properties file will be found under the resources folder.

* Summary of set up
* Running the application - run the module 
```
#!xml

brightworks-admin-webapp
```
 using 
```
#!xml

mvn jetty:run
```
and 
```
#!xml

brightworks-client-webapp
```
 using  
```
#!xml

mvn jetty:run
```
 
NOTE that the admin module should be running in order for the users to login using the client-webapp.

* Database configuration - Look under the resources for the properties file of each web application
* How to run tests - mvn install(AS OF NOW, NO TEST YET)

* Deployment instructions - Add the ssh key of the said server to the deployment keys of this repository and clone. Once completed, Perform a 
```
#!xml

mvn:clean install
```
 on the root project. Once the build is successfully finished, (re-read Configuration)

# Package summary #

* ** brightworks-commons**
 -composed of the services,and models that all modules used. Which includes the Security
* ** brightworks-ims-core**
 -Has all the inventory management and order management services, dtos, and models for the inventory and management module
* **brightworks-admin-core**
 -Has all the administrative/user management services, dtos, and models for the admin and user management module. 
* **brightworks-client-webapp**
 -The Web application for the Inventory and management and Order Management.This web app uses the brightworks-ims-core
* **brightworks-admin-webapp**
 -The web application for the admin, and user management module. This web app uses the brightworks-admin-core module
*

# Steps to start fully using the application for both admin and client? #
* The default admin account has a username=admin, and password=123qwerty. 
* Go To Add Branch, Create Branch(Which will automatically create a schema in the client database)
* Go to Add User, Create a user and assign a username,password, and branch assignment.
* Once Done adding User. you can now login at the client web application.

### Who do I talk to? ###
* Kyel John M. David
* Nathaniel Kurt Lagare
